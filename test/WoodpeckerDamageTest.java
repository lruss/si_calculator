package test;

import org.junit.Test;

import com.alamon.si_calculator.inspection_data.CrossSection;
import com.alamon.si_calculator.inspection_data.PoleData;
import com.alamon.si_calculator.inspection_data.damage.WoodPeckerCavityDamage;
import com.alamon.si_calculator.math.utils.Utils;

public class WoodpeckerDamageTest {

	@SuppressWarnings("unused")
	@Test
	//@Ignore
	public void woodpeckerDamageTest() {
		PoleData poleData = new PoleData("PoleID 1", 0, 10);
		CrossSection crossSection = new CrossSection(poleData);
		double angleToOpening = 45.0;
		
		double angleBetweenRadiusesToMissingCircumference = 35.62;
		double missingCircumference = Utils.calculateArcLengthByDiameterAndAngle(angleBetweenRadiusesToMissingCircumference, 
				crossSection.getOutsideDiameter());
		System.out.println("Missing Circumference for Woodpecker damage = " + missingCircumference);
		double shellThickness = 1.82;
		double nestDiameter = 1.58;
		WoodPeckerCavityDamage woodPeckerDamage = new WoodPeckerCavityDamage(crossSection, angleToOpening, 
				missingCircumference, shellThickness, nestDiameter);
	}

}
