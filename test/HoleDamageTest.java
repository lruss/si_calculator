package test;

import org.junit.Ignore;
import org.junit.Test;

import com.alamon.si_calculator.exceptions.ErrorMessages;
import com.alamon.si_calculator.exceptions.InvalidEntryException;
import com.alamon.si_calculator.inspection_data.CrossSection;
import com.alamon.si_calculator.inspection_data.PoleData;
import com.alamon.si_calculator.inspection_data.damage.HoleDamage;

public class HoleDamageTest {

	/**
	 * This test is an extended version of standard d-calc hole damage implementation -
	 * it allows evaluation of a hole damage with centerline not going through cross section
	 * circumference center and/or hole not going all the way through the pole.
	 * It takes four parameters:
	 * angleToOpening - angle between base diameter and hole center line
	 * arcFromBaseToCenterLine
	 * depth
	 * double holeDiameter
	 * Corresponding dwg file: holeDamage3, no analog in d-calc
	 */
	//@Test
	@Ignore
	public void holeDamageEnhancedTest() {
		PoleData poleData = new PoleData("PoleID 1", 0, 10);
		CrossSection crossSection = new CrossSection(poleData);
		
		double angleToOpening = 45;
		double arcFromBaseToCenterLine = 5.77;
		Double depth = 5.98;
		double holeDiameter = 2.0;		
		@SuppressWarnings("unused")
		HoleDamage holeDamage = null;
		try {
			holeDamage = new HoleDamage(crossSection, angleToOpening, arcFromBaseToCenterLine, depth, holeDiameter, true);
		} catch (InvalidEntryException e) {
			ErrorMessages.addErrorMessages(e.getClass().getName() + ": " + e.getMessage(), true);
			System.out.println(ErrorMessages.printMessages());
		}
	}
	
	@SuppressWarnings("unused")
	//@Test
	@Ignore
	/**
	 * This test resembles standard d-calc hole damage scenario with two parameters - angle to opening and hole diameter
	 * D-calc file: holeDamage1
	 */	
	public void holeDamageBasicTest1() {
		PoleData poleData = new PoleData("PoleID 1", 0, 10);
		CrossSection crossSection = new CrossSection(poleData);		
		double angleToOpening = 30;		
		double holeDiameter = 2.0;		
		HoleDamage holeDamage = null;		
		try {
			holeDamage = new HoleDamage(crossSection, angleToOpening, holeDiameter, true);
		} catch (InvalidEntryException e) {
			ErrorMessages.addErrorMessages(e.getClass().getName() + ": " + e.getMessage(), true);
			System.out.println(ErrorMessages.printMessages());
		}
	}
	
	@SuppressWarnings("unused")
	@Test
	//@Ignore
	/**
	 * This test resembles standard d-calc hole damage scenario with two parameters - angle to opening and hole diameter
	 * D-calc file: holeDamage2
	 * Dwg file: holeDamage2
	 */	
	public void holeDamageBasicTest2() {
		PoleData poleData = new PoleData("PoleID 1", 0, 10);
		CrossSection crossSection = new CrossSection(poleData);		
		double angleToOpening = 120;		
		double holeDiameter = 2.0;		
		HoleDamage holeDamage = null;
		try {
			holeDamage = new HoleDamage(crossSection, angleToOpening, holeDiameter, true);
		} catch (InvalidEntryException e) {			
			ErrorMessages.addErrorMessages(e.getClass().getName() + ": " + e.getMessage(), true);
			System.out.println(ErrorMessages.printMessages());
		}
	}
	
	@SuppressWarnings("unused")
	//@Test
	@Ignore
	/**
	 * This test resembles standard d-calc hole damage scenario with two parameters - angle to opening and hole diameter
	 * For this test, set noOfChords in varConstantsArr.json to 2000 - the hole is parallel to base diameter, and accurate
	 * evaluation needs more data/
	 * D-calc file: holeDamage4
	 * Dwg file: holeDamage4
	 */	
	public void holeDamageBasicTest4() {
		PoleData poleData = new PoleData("PoleID 1", 0, 10);
		CrossSection crossSection = new CrossSection(poleData);		
		double angleToOpening = 0;		
		double holeDiameter = 2.0;		
		HoleDamage holeDamage = null;
		try {
			holeDamage = new HoleDamage(crossSection, angleToOpening, holeDiameter, true);
		} catch (InvalidEntryException e) {
			ErrorMessages.addErrorMessages(e.getClass().getName() + ": " + e.getMessage(), true);
			System.out.println(ErrorMessages.printMessages());
		}
	}
	
	@SuppressWarnings("unused")
	//@Test
	@Ignore
	/**
	 * This test resembles standard d-calc hole damage scenario with two parameters - angle to opening and hole diameter
	 * D-calc file: holeDamage5
	 * Dwg file: holeDamage5
	 */	
	public void holeDamageBasicTest5() {
		PoleData poleData = new PoleData("PoleID 1", 0, 10);
		CrossSection crossSection = new CrossSection(poleData);		
		double angleToOpening = 90;		
		double holeDiameter = 2.0;		
		HoleDamage holeDamage = null;
		try {
			holeDamage = new HoleDamage(crossSection, angleToOpening, holeDiameter, true);
		} catch (InvalidEntryException e) {
			ErrorMessages.addErrorMessages(e.getClass().getName() + ": " + e.getMessage(), true);
			System.out.println(ErrorMessages.printMessages());
		}
	}



}
