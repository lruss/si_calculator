package test;

import org.junit.Ignore;
import org.junit.Test;

import com.alamon.si_calculator.inspection_data.CrossSection;
import com.alamon.si_calculator.inspection_data.PoleData;
import com.alamon.si_calculator.inspection_data.damage.HeartDamage;

@SuppressWarnings("unused")
public class HeartDamageTest {

	@Test
//	@Ignore
	public void heartDamageTest() {
		PoleData poleData = new PoleData("PoleID 1", 0, 10);
		CrossSection crossSection = new CrossSection(poleData);

		// HeartDamage heartDamage4 = new HeartDamage(crossSection, 2, 10, 0);
		// HeartDamage heartDamage4 = new HeartDamage(crossSection, 3, 10, 0);
		// HeartDamage heartDamage4 = new HeartDamage(crossSection, 0, 0, 0);

		//ID = 10, no offset (OK!), dcalc = 51.6
		// HeartDamage heartDamage4 = new HeartDamage(crossSection, 1, 11, 0);

		// ID = 8, no offset (OK!), dcalc =	 80.2
		HeartDamage heartDamage4 = new HeartDamage(crossSection, .6, 4.6, 62); 
		//HeartDamage heartDamage4 = new HeartDamage(crossSection, 1.8, 3.39, 45);
	//	HeartDamage heartDamage4 = new HeartDamage(crossSection, .6, 4, 62, true);

		//ID = 4, no offset (OK!)
		// HeartDamage heartDamage4 = new HeartDamage(crossSection, 4, 8, 0);

		// RingDamage ringDamage4 = new RingDamage(crossSection, 2);

	
		double remainingIntegrity = crossSection.getRemainingStrength();
		System.out.println("LR TEST77-7: original section modulus = " + crossSection.getIntactSectionModulus()
				+ ", ttlRemaining Strength = " + remainingIntegrity + ", remaining sectionModulus = "
				+ crossSection.getRemainingSectionModulus());

	}

}
