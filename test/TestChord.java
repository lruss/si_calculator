package test;

import java.util.Collections;

import org.junit.Ignore;
import org.junit.Test;

import com.alamon.resource_reader.json.LoadStaticResources;
import com.alamon.si_calculator.inspection_data.CrossSection;
import com.alamon.si_calculator.inspection_data.PoleData;
import com.alamon.si_calculator.inspection_data.damage.HeartDamage;
import com.alamon.si_calculator.inspection_data.damage.WedgeDamage;
import com.alamon.si_calculator.math.geometry.Chord;
import com.alamon.si_calculator.math.geometry.Coordinates;
import com.alamon.si_calculator.math.geometry.Line;
import com.alamon.si_calculator.math.geometry.Pocket;
import com.alamon.si_calculator.math.utils.ChordBreakComparator;
import com.alamon.si_calculator.math.utils.Units;
import com.alamon.si_calculator.math.utils.Utils;

public class TestChord {

	// @Test
	@Ignore
	public void compareGaps() {
		CrossSection crossSection = new CrossSection(new PoleData("Pole 1", 45, 12));
		Chord chord = new Chord(0, 0, crossSection);
		chord.setLength();
		// chord.addGap(2, 4);
		// chord.addGap(5, 10);
		// chord.addGap(1, 3);
		Collections.sort(chord.getGaps(), new ChordBreakComparator());
		assert (chord.getGaps().get(0).getStartPoint() == 1);
		assert (chord.getGaps().get(1).getStartPoint() == 2);
		assert (chord.getGaps().get(2).getStartPoint() == 5);
	}

	// @Test
	@SuppressWarnings("unused")
	@Ignore
	public void checkGapPoints() {
		CrossSection crossSection = new CrossSection(new PoleData("Pole 1", 45, 10));
		Chord chord = new Chord(0, 0, crossSection);
		// chord.setLength(10);
		// chord.addGap(2, 4);
		// chord.addGap(2, 4);
		// chord.addGap(11, 15);
		// chord.addGap(1, 3);
	}

	// @Test
	@Ignore
	public void testSuperimposeGaps() {
		CrossSection crossSection = new CrossSection(new PoleData("Pole 1", 45, 10));
		Chord chord = new Chord(0, 0, crossSection);
		chord.setLength();
		// chord.addGap(2, 4);
		// chord.addGap(2, 4);
		// chord.addGap(5, 7);
		// chord.addGap(1, 3);
		// chord.addGap(7, 8);
		// chord.addGap(1, 9);
		// chord.addGap(9.2, 9.5);
		// chord.addGap(0.2, 0.5);
		// chord.addGap(0.3, 0.7);
		//
		System.out.println("Gaps size before superimposeGaps = " + chord.getGaps().size());
		for (Pocket gap : chord.getGaps()) {
			System.out.println(gap.toString());
		}
		chord.superimposeGaps();
		System.out.println("Gaps size after superimposeGaps = " + chord.getGaps().size());
		System.out.println("Gaps are superimposeGaps : " + chord.getGaps().toString());
		for (Pocket gap : chord.getGaps()) {
			System.out.println(gap.toString());
		}

		System.out.println("Sum of gaps = " + chord.getTotalGapsLength());
		// System.out
		// .println("Relative integrity = " +
		// Units.setDoublePrecision(chord.getRelativeIntegrity()) * 100 + "%");
	}

	// @Test
	@Ignore
	public void getNoOfCordsFromVarContantsArrJsonShort() {
		System.out.println("NoOfChords = "
				+ LoadStaticResources.getFieldValue("varConstantsArr.json", "name", "noOfChords", "value"));
	}

	// @Test
	@Ignore
	public void getPrecisionFromVarContantsArrJsonShort() {
		System.out.println("Precision = "
				+ LoadStaticResources.getFieldValue("varConstantsArr.json", "name", "precision", "value"));
	}

	// @Test
	@SuppressWarnings("unused")
	@Ignore
	public void calculateChordLengthByEndCoordinates() {
		// CrossSection crossSection = new CrossSection("CrossSectionID-1", 10,
		// 45);
		CrossSection crossSection = new CrossSection(new PoleData("Pole 1", 45, 10));
		crossSection.setChordToBaseDiamAngle(45);
		System.out.println("LR TEST initiated No of chords: " + crossSection.getChords().size());
		for (Chord chord : crossSection.getChords()) {
			System.out.println("LR TEST Diameter = " + 10 + ", chordNo = " + chord.getChordNo()
					+ ", dist from center = " + chord.getDistanceFromCrossSectionCenter() + ", TOTAL chord length = "
					+ chord.getLength());
		}
		HeartDamage heartDamage = new HeartDamage(crossSection, 2, 10, 45);
		System.out.println("LR TEST heartDamageChordLength = "
				+ Utils.getDistanceBetweenPointsByCoordinates(crossSection.getChords().get(1).getEndCoordinates()));
	}

	// @Test
	@SuppressWarnings("unused")
	@Ignore
	public void testLineEquation() {
		PoleData poleData = new PoleData("Pole 1", 45, 10);
		CrossSection crossSection = new CrossSection(poleData);
		crossSection.setChordToBaseDiamAngle(45);
		// crossSection.setChordToBaseDiamAngle(0);
		Chord chord = crossSection.getChords().get(0);
		double angle = crossSection.getChordToBaseDiamAngle();
		System.out.println("LR TEST cos 45 " + Units.cos(angle));

		double offsetFromXaxis = chord.getDistanceFromCrossSectionCenter() / Units.cos(angle);
		// double offsetFromXaxis = 0;

		double chordX = Units.cos(angle * chord.getDistanceFromCrossSectionCenter() - offsetFromXaxis);
		double chordY = Units.tan(angle) * chordX + offsetFromXaxis;
		double circleDiameter = 4;
		// 
		double circleCenX = 2.12;// 0;
		double circleCenY = 2.12;// 0;

		double a = 1 + Math.pow(Units.tan(angle), 2);
		double b = -2 * circleCenX + 2 * Units.tan(angle) * offsetFromXaxis - 2 * Units.tan(angle) * circleCenY;
		double c = Math.pow(circleCenX, 2) + Math.pow(offsetFromXaxis, 2) - 2 * offsetFromXaxis * circleCenY
				+ Math.pow(circleCenY, 2) - Math.pow((circleDiameter / 2), 2);

		double interX1 = (-b + Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / (2 * a);
		double interX2 = (-b - Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / (2 * a);
		double interY1 = Units.tan(angle) * interX1 + offsetFromXaxis;
		double interY2 = Units.tan(angle) * interX2 + offsetFromXaxis;
		Coordinates[] coordinates = new Coordinates[2];
		coordinates[0] = new Coordinates();
		coordinates[1] = new Coordinates();
		coordinates[0].setX(interX1);
		coordinates[0].setY(interY1);
		coordinates[1].setX(interX2);
		coordinates[1].setY(interY2);

		System.out.println("LR TEST " + chord.getDistanceFromCrossSectionCenter() + " chord offset = " + offsetFromXaxis
				+ "\nintesection cooridantes: X1 = " + interX1 + ", Y1 = " + interY1 + "\nX2 = " + interX2 + ", Y2 = "
				+ interY2);
	}

	
	// @Test
	@Ignore
	// Works for center hole (same center with cross section center) only
	public void calculateCircularMomentOfInertia() {
		double insideD1 = 0;
		double outsideD1 = 12;
		double insideD2 = 9;
		double outsideD2 = 12;
		double distanceFromCenter = .5 * Units.cos(90);
		double moment1 = Math.PI * ((Math.pow(outsideD1, 4) - Math.pow(insideD1, 4)) / 64);
		double moment2 = (Math.PI * ((Math.pow(outsideD2, 4) - Math.pow(insideD2, 4))) / 64);
		System.out.println("Intact: " + moment1 + ", affected: " + moment2 + ", ratio: "
				+ ((moment2 / (moment1 + distanceFromCenter * 340))));
		// THIS WORKS!!!!!!!!!!!!!!!!!!
	}

	// @Test
	@Ignore
	public void calculateMomentOfInertia() {
		double ID1 = 0;
		double OD1 = 12;
		double ID2 = 8;
		double OD2 = 12;

		double moment1 = Math.PI * ((Math.pow(OD1, 4) - Math.pow(OD1, 4)) / 64);
		double moment2 = (Math.PI * ((Math.pow(OD2, 4) - Math.pow(ID2, 4))) / 64);
		System.out.println("Intact: " + moment1 + ", affected: " + moment2 + ", ratio: " + (moment2 / moment1));

		moment1 = ((Math.PI * Math.pow(ID1, 3) * 2) / 8);
		moment2 = (Math.PI * Math.pow(ID2, 3) * 2) / 8;
	}

	//@Test
	@Ignore
	public void testLineIntersection()	{
		Coordinates[] endCoordinates1 = { new Coordinates(3.09, 0.96), new Coordinates(-1.78, -2.63) };
		System.out.println(
				Utils.getLineAngleByEndPoints(endCoordinates1)
				);
		
		Coordinates[] endCoordinates2 = { new Coordinates(4.28, 0.34), new Coordinates(-1.72, 2.63) };
		System.out.println(
				Utils.getLineAngleByEndPoints(endCoordinates2)
				);
		CrossSection crossSection = new CrossSection(new PoleData("Pole1 ID", 0, 12));
		
		//Coordinates[] coordinates2 = new Coordinates[] {new Coordinates(5, 0), new Coordinates(10,0)};
		//TODO set distanceBetweenChords for line1
		Line line1 = new Line(endCoordinates1, crossSection, "Line1");	
		//TODO set distanceBetweenChords for line2
		Line line2 = new Line(endCoordinates2, crossSection, "Line2");
		Coordinates intersection = line1.calculateIntersectionWithAnotherChordByCoordinates(line2);
		System.out.println("Intersection: X = " + intersection.getX() + ", Y = " + intersection.getY());
	}
	
	//@Test
	@Ignore
	public void testLineNonIntersection()	{
		//Coordinates[] endCoordinates1 = { new Coordinates(3.6095, 1.6230), new Coordinates(-1.7713, -2.5425) };
		Coordinates[] endCoordinates1 = { new Coordinates(3.8156, 3.2313), new Coordinates(-3.7000, 1.1000) };
		System.out.println(
				Utils.getLineAngleByEndPoints(endCoordinates1)
				);
		
		Coordinates[] endCoordinates2 = { new Coordinates(-2.92, -2.29), new Coordinates(-1.07, -3.57) }; //Does NOT intersect
		//Coordinates[] endCoordinates2 = { new Coordinates(4.2835, 0.3413), new Coordinates(-1.7160, 2.6284) }; //Does intersect
		//Coordinates[] endCoordinates2 = { new Coordinates(3.4694, 3.6004), new Coordinates(-1.3000, -2.5000) }; //Does intersect
		System.out.println(
				Utils.getLineAngleByEndPoints(endCoordinates2)
				);
		CrossSection crossSection = new CrossSection(new PoleData("Pole1 ID", 0, 12));
		
		//Coordinates[] coordinates2 = new Coordinates[] {new Coordinates(5, 0), new Coordinates(10,0)};
		//TODO set distanceBetweenChords for line1
		Line line1 = new Line(endCoordinates1, crossSection, "Line1");	
		//TODO set distanceBetweenChords for line2
		Line line2 = new Line(endCoordinates2, crossSection, "Line2");
		Coordinates intersectionCoordinates = line1.calculateIntersectionWithAnotherChordByCoordinates(line2);
		System.out.println("Intersection: X = " + intersectionCoordinates.getX() + ", Y = " + intersectionCoordinates.getY());
		System.out.println("Does Intersect : " + line1.doesIntersect(intersectionCoordinates));		
	}
	
	//@Test
	@SuppressWarnings("unused")
	@Ignore
	public void wedgeDamageTest()	{
		CrossSection crossSection = new CrossSection(new PoleData("Pole1 ID", 0, 10));
		/////////////////////////////////
		WedgeDamage wdgeDamage = new WedgeDamage (crossSection, 45, 5.23, 2.5);
		System.out.println(crossSection.toString());
		/*
		Readings:
			angleBetweenSides	107.4866094232499	
			line1ToBaseDiameterAngle	171.25669528837506	
			line2ToBaseDiameterAngle	98.74330471162496	
			x1_1	4.828853393059938	
			y1_1	1.2969868574251333	
			x1_2	1.767766952966368	
			y1_2	0.8262067618838971 - WRONG	
	*/
		////////////////////////
		//WedgeDamage wageDamage = new WedgeDamage (crossSection, 70, 5.23, 2.5);
		/*
		 Readings:
		angleBetweenSides	107.4866094232499	
		line1ToBaseDiameterAngle	196.25669528837506	
		line2ToBaseDiameterAngle	123.74330471162496	
		x1_1	3.8282971014030287	
		y1_1	3.2162309157442612	
		x1_2	0.8550503583141715	
		y1_2	4.083230279523753	- wrong
		*/
		////////////////////////////

	}
	
	@Test
	@SuppressWarnings("unused")
	//@Ignore
	public void testLineCoordinates()	{
		double chordToBaseAngle = 0;
		double crossSectionDiameter = 10;
		double R = crossSectionDiameter/2;
		PoleData poleData = new PoleData("PD_ID-1", chordToBaseAngle, crossSectionDiameter);
		CrossSection crossSection = new CrossSection(poleData);
		//Diameter
		Line line = new Line(new Coordinates[] {new Coordinates(crossSectionDiameter/2, 0), new Coordinates(-R,0)}, crossSection, "baseDiameter");
		double rotationAngle = 90;
		//Rotate line 
		
		Coordinates newStartCoordinates = new Coordinates( 
				Units.cos(rotationAngle) * R - line.getEndCoordinates()[0].getX(),
				Units.sin(rotationAngle) * R - line.getEndCoordinates()[0].getY()
//				Units.cos(rotationAngle) * R,
//				Units.sin(rotationAngle) * R
				);
		Coordinates newEndCoordinates = new Coordinates(
				-(Units.cos(rotationAngle) * R - line.getEndCoordinates()[0].getX()),
				-(Units.sin(rotationAngle) * R - line.getEndCoordinates()[0].getY())
				);
		
		Line rotatedLine = new Line(new Coordinates[] {newStartCoordinates, newEndCoordinates}, crossSection, "rotated baseDiameter");
		System.out.println("LR TEST PAUSE");
	}
}
