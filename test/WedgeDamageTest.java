package test;

import org.junit.Test;

import com.alamon.si_calculator.inspection_data.CrossSection;
import com.alamon.si_calculator.inspection_data.PoleData;
import com.alamon.si_calculator.inspection_data.damage.WedgeDamage;
import com.alamon.si_calculator.math.utils.Utils;

public class WedgeDamageTest {

	@SuppressWarnings("unused")
	@Test
	public void wedgeDamageTest() {
		PoleData poleData = new PoleData("PoleID 1", 0, 10);
		CrossSection crossSection = new CrossSection(poleData);
		double segmentLength = Utils.getSegmentLengthBySideAngleAndDiameter(crossSection.getOutsideDiameter(), 60);
		WedgeDamage damage = new WedgeDamage(crossSection, 45, segmentLength, 2.5);
		double remainingIntegrity = crossSection.getRemainingStrength();
		System.out.println(poleData.toString());
		System.out.println("Cross section with WedgeDamage remaining strength = " + remainingIntegrity);
	}

}
