package test;

import org.junit.Ignore;
import org.junit.Test;

import com.alamon.si_calculator.inspection_data.CrossSection;
import com.alamon.si_calculator.inspection_data.PoleData;
import com.alamon.si_calculator.inspection_data.damage.SliceDamage;
import com.alamon.si_calculator.math.utils.Utils;

public class SliceDamageTest {

	@SuppressWarnings("unused")
	@Test
	//@Ignore
	public void calculateMissingWidth() {
		PoleData poleData = new PoleData("PoleID 1", 0, 10);
		CrossSection crossSection = new CrossSection(poleData);
		Double angleToDamage = 45.0;
		Double missingDepth = 1.0978;
		double segmentLength = Utils.getSegmentLengthBySideAngleAndDiameter(crossSection.getOutsideDiameter(), 60);
		
		//Given Missing Depth
		try {
			SliceDamage damage = new SliceDamage(crossSection, angleToDamage, missingDepth, null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		double remainingIntegrity = crossSection.getRemainingStrength();
		System.out.println(poleData.toString());
		System.out.println("Cross section with SliceDamage remaining strength = " + remainingIntegrity);
	}
	
	//@Test
	@SuppressWarnings("unused")
	@Ignore
	public void calculateMissingDepth()	{
		PoleData poleData = new PoleData("PoleID 1", 0, 10);
		CrossSection crossSection = new CrossSection(poleData);	
		
		//Given Missing Width
		Double angleToDamage = 45.0;
		Double missingWidth = 6.25;
				try {
					SliceDamage damage = new SliceDamage(crossSection, angleToDamage, null, missingWidth);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				double remainingIntegrity = crossSection.getRemainingStrength();
				System.out.println(poleData.toString());
				System.out.println("Cross section with SliceDamage remaining strength = " + remainingIntegrity);
	}

}
