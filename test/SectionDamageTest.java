package test;

import org.junit.Test;

import com.alamon.si_calculator.inspection_data.CrossSection;
import com.alamon.si_calculator.inspection_data.PoleData;
import com.alamon.si_calculator.inspection_data.damage.SectionDamage;
import com.alamon.si_calculator.math.utils.Utils;

public class SectionDamageTest {

	@SuppressWarnings("unused")
	@Test
	public void sectionDamageTest() {
		PoleData poleData = new PoleData("PoleID 1", 0, 10);
		CrossSection crossSection = new CrossSection(poleData);
		double shellThickness = 0.836;
		double missingDepth = 2;
		double missingCircumference = Utils.getSegmentLengthBySideAngleAndDiameter(crossSection.getOutsideDiameter() - shellThickness * 2, 73.8);
		SectionDamage sectionDamage = new SectionDamage(crossSection, 45, missingCircumference, missingDepth, shellThickness);
	}

}
