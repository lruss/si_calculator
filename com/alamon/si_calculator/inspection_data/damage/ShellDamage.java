package com.alamon.si_calculator.inspection_data.damage;

import com.alamon.si_calculator.inspection_data.CrossSection;
import com.alamon.si_calculator.math.geometry.Chord;
import com.alamon.si_calculator.math.geometry.Coordinates;

public class ShellDamage extends Damage {	
	private double ringDamageDepth;
	private double radius;
	private Coordinates ringDamageCenterCoordinates;
	private static final String damageType = "Ring Damage";

	/**
	 * 
	 * @param crossSection
	 * @param ringDamageDepth
	 */
	public ShellDamage(CrossSection crossSection, double ringDamageDepth) {
		super(crossSection);		
		this.ringDamageDepth = ringDamageDepth;
		this.crossSection = crossSection;		
		try {
		setCenterCoordinates();
		} catch (Exception e)	{
			System.out.println(e.getMessage());
		}
		setPockets();
		crossSection.setStructuralIntegrity();
		crossSection.setRemainingSectionModulus();
		System.out.println("Damage of type [" + damageType + "] added to Cross Section. Remaining strength for cross section " +
		crossSection.getCrossSectionID() + " = " + crossSection.getRemainingStrength());
	}

	private void setCenterCoordinates() throws Exception {				
		if (ringDamageDepth  >= crossSection.getOutsideDiameter() / 2) {
			throw new Exception(
					"ERROR: invalid parameters - ring depth may not be equal or  greater than cross section diameter.");
		}
		radius = (crossSection.getOutsideDiameter() - 2 * ringDamageDepth) / 2;		
		System.out.println("Ring damage radius = " + radius);
		ringDamageCenterCoordinates = new Coordinates(0, 0);		
		if (radius == 0 && ringDamageDepth == 0) {
			ringDamageCenterCoordinates = new Coordinates(0, 0);
			return;
		}
	
	}

	public void setPockets() {
		System.out.println("Pocket : " + " center coordinates: " + ringDamageCenterCoordinates.getX() + ", "
				+ ringDamageCenterCoordinates.getY());
		for (Chord chord : crossSection.getChords()) {
			if (chord == null) {
				continue;
			}
			Coordinates[] intersectionCoordinates = crossSection
					.calculateChordWithCircleIntesectionPoints(ringDamageCenterCoordinates, chord, radius * 2, null);
			
			/*
			 * The following scenario indicates that chord does not intersect with solid/intact portion of the 
			 * cross section, and - since for ring damage calculation is done by reversing intact and damaged
			 * areas - the entire chord is a gap.
			 */
			if (Double.isNaN(intersectionCoordinates[0].getX()) 
					|| Double.isNaN(intersectionCoordinates[0].getY())
					|| Double.isNaN(intersectionCoordinates[1].getX())
					|| Double.isNaN(intersectionCoordinates[1].getY())) {
				//System.out.println("LR TEST chordNo " + chord.getChordNo() + " does not intersect, remaining intact " + chord.getRemainingIntact());
				Coordinates[] intersectionCoordinates1 = {chord.getEndCoordinates()[0],  chord.getEndCoordinates()[1]};
				chord.addGap( intersectionCoordinates1, chord.getEndCoordinates()[0]);	
				//System.out.println("RingDamage chordNo " + chord.getChordNo() + " total gap added, ttl length for chord = " + 
				//chord.getTotalGapsLength() + ", remaininglIntact = " + chord.getRemainingIntact());
				
				continue;
			}			
		
			Coordinates[] intersectionCoordinates1 = {chord.getEndCoordinates()[0],  intersectionCoordinates[0]};
			
			chord.addGap( intersectionCoordinates1, chord.getEndCoordinates()[0]);	
			//System.out.println("RingDamage chordNo " + chord.getChordNo() + " gap added, ttl length for chord = " + chord.getTotalGapsLength());
			
			Coordinates[] intersectionCoordinates2 = {intersectionCoordinates[1],  chord.getEndCoordinates()[1]};
			chord.addGap(intersectionCoordinates2, chord.getEndCoordinates()[0]);	
			//System.out.println("RingDamage chordNo " + chord.getChordNo() + " gap added, ttl length for chord = " + chord.getTotalGapsLength());
			//			System.out.println("Pocket chord No." + chord.getChordNo() + ", pocket center = "
//					+ ringDamageCenterCoordinates.getX() + " " + ringDamageCenterCoordinates.getY() + ", offset: "
//					+ chord.getDistanceFromCrossSectionCenter() + ", intersection coordinates: "
//					+ intersectionCoordinates[0].getX() + ", " + intersectionCoordinates[0].getY() + "; "
//					+ intersectionCoordinates[1].getX() + ", " + intersectionCoordinates[1].getY() + "\n");
			
		}

	}

	public Coordinates getRingDamageCenterCoordinates() {
		return ringDamageCenterCoordinates;
	}

	public double getRadius() {
		return radius;
	}
}
