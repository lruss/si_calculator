package com.alamon.si_calculator.inspection_data.damage;

import com.alamon.si_calculator.inspection_data.CrossSection;
import com.alamon.si_calculator.math.geometry.Chord;
import com.alamon.si_calculator.math.geometry.Coordinates;
import com.alamon.si_calculator.math.geometry.Line;
import com.alamon.si_calculator.math.utils.Units;
import com.alamon.si_calculator.math.utils.Utils;

public class SliceDamage extends Damage {
	private Double missingDepth;
	private static final String damageType = "Slice Damage";
	private Line damageBorder;
	private double angleToDamage;
	private Double missingWidth;
	private double angleBetweenRadiusesAndChordEnds;

	public SliceDamage(CrossSection crossSection, Double angleToDamage, Double missingDepth, Double missingWidth)
			throws Exception {
		super(crossSection);
		if (missingDepth != null && missingWidth != null) {
			throw new Exception(
					"Only one of two - missing depth or missing width - is allowed, the other one must be null.");
		}
		if (missingDepth == null && missingWidth == null) {
			throw new Exception("One of two - missing depth or missing width - must be not null.");
		}

		this.angleToDamage = angleToDamage;
		this.missingDepth = missingDepth;
		this.missingWidth = missingWidth;

		setDamageBorder();
		setPockets();
		crossSection.setStructuralIntegrity();
		crossSection.setRemainingSectionModulus();
		super.addDamageToCrossSection(this);
		System.out.println(
				"Damage of type [" + damageType + "] added to Cross Section. Remaining strength for cross section "
						+ crossSection.getCrossSectionID() + " = " + crossSection.getRemainingStrength());
	}

	private void setDamageBorder() {
		//		double angleBetweenRadiusesToMissingCircumference = (360 * missingCircumference)
		//				/ (Math.PI * crossSection.getOutsideDiameter());
		if (this.missingWidth == null) {
			this.missingWidth = Math.sqrt(Math.pow(crossSection.getOutsideDiameter() / 2, 2)
					- Math.pow((crossSection.getOutsideDiameter() / 2 - missingDepth), 2)) * 2;
			angleBetweenRadiusesAndChordEnds = Units.aSin((missingWidth / 2) /
					(crossSection.getOutsideDiameter() / 2)) * 2;
		} else {
			angleBetweenRadiusesAndChordEnds = Units.aSin((missingWidth / 2) / (crossSection.getOutsideDiameter() / 2))
					* 2;
			this.missingDepth = crossSection.getOutsideDiameter() / 2 - Units.cos(angleBetweenRadiusesAndChordEnds / 2);
		}
		double angleRadius1ToBaseDiameter = angleToDamage - angleBetweenRadiusesAndChordEnds / 2;
		double angleRadius2ToBaseDiameter = angleToDamage + angleBetweenRadiusesAndChordEnds / 2;
		//Will use radiuses to damageBorder line ends to construct a borderDamage Line object with end points @ intersection 
		//of radiuses and cross section circumference		
		Coordinates radius1EndPoint = (Utils.calculateChordWithCircleIntesectionPoints(null, angleRadius1ToBaseDiameter, crossSection.getOutsideDiameter(), null)[0]);		
		Coordinates radius2EndPoint = (Utils.calculateChordWithCircleIntesectionPoints(null, angleRadius2ToBaseDiameter, crossSection.getOutsideDiameter(), null)[0]);		
		damageBorder = new Line(new Coordinates[] {radius1EndPoint, radius2EndPoint}, crossSection, "Damage Border");
	}

	@Override
	public void setPockets() {
		for (Chord chord : crossSection.getChords()) {
			if (chord == null) {
				continue;
			}			
			Coordinates intersectionCoordinates = damageBorder.calculateIntersectionWithAnotherChordByCoordinates(chord);
			if(!damageBorder.doesIntersect(intersectionCoordinates))	{
				continue;
			}
			chord.addGap(new Coordinates[] {chord.getEndCoordinates()[0], intersectionCoordinates}, chord.getEndCoordinates()[0]);			
		}
	}
	//	
	//	@Override
	//	public String toString()	{
	//		StringBuffer sb = new StringBuffer("\n\tWedgeDamage: \n");
	//		sb.append("\n\t\tSide1: " + side1.toString());
	//		sb.append("\n\t\tSide2: " + side2.toString());
	//		sb.append("\n\t\tcenterLineToBaseDiameterAngle: " + centerLineToBaseDiameterAngle);
	//		sb.append(", Missing Circumference: " + missingCircumference);
	//		sb.append(", depth: " + depth);
	//		return sb.toString();
	//	}

}
