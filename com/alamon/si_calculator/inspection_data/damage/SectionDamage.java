package com.alamon.si_calculator.inspection_data.damage;

import com.alamon.si_calculator.inspection_data.CrossSection;
import com.alamon.si_calculator.math.geometry.Coordinates;
import com.alamon.si_calculator.math.geometry.Line;
import com.alamon.si_calculator.math.utils.Units;
import com.alamon.si_calculator.math.utils.Utils;

public class SectionDamage extends Damage {

	private double angleToOpening;
	private double missingCircumference;
	private double missingDepth;
	private double shellThickness;
	private static final String damageType = "Section Damage";
	private Line side1;
	private Line side2;
	private double sideLength;
	private Line innerBoundary;
	private Line outerBoundary;
	private double innerCircleDiameter;
	private double outerCircleDiameter;

	/**
	 * 
	 * @param crossSection
	 * @param angleToOpening
	 * @param missingCircumference
	 * @param missingDepth
	 * @param shellThickness
	 */
	public SectionDamage(CrossSection crossSection, double angleToOpening, double missingCircumference,
			double missingDepth, double shellThickness) {
		super(crossSection);
		this.angleToOpening = angleToOpening;
		this.missingCircumference = missingCircumference;
		this.missingDepth = missingDepth;
		this.shellThickness = shellThickness;
		this.outerCircleDiameter = crossSection.getOutsideDiameter() - shellThickness * 2;
		this.innerCircleDiameter	= 	crossSection.getOutsideDiameter() - missingDepth * 2;
//		this.outerCircleDiameter = crossSection.getOutsideDiameter() - shellThickness * 2;
//		this.innerCircleDiameter = crossSection.getOutsideDiameter() - shellThickness * 2 - missingDepth * 2;
		setBoundaries();
		setPockets();
		crossSection.setStructuralIntegrity();
		crossSection.setRemainingSectionModulus();
		super.addDamageToCrossSection(this);
		System.out.println(
				"Damage of type [" + damageType + "] added to Cross Section. Remaining strength for cross section "
						+ crossSection.getCrossSectionID() + " = " + crossSection.getRemainingStrength());
	}

	private void setBoundaries() {
		double R = crossSection.getOutsideDiameter() / 2 - shellThickness;
		// A3
		double angleBetweenRadiusesToMissingCircumference = (360 * missingCircumference) / (Math.PI * 2 * R);
		sideLength = missingDepth;
		// A4
		double angleBetweenSides = angleBetweenRadiusesToMissingCircumference;
		double side1ToBaseDiameterAngle = angleToOpening - angleBetweenSides / 2;
		double side2ToBaseDiameterAngle = angleBetweenSides + side1ToBaseDiameterAngle;

		// side1 start point (intersection with cross section outline):
		double side1StartPointX = Units.cos(angleToOpening - angleBetweenRadiusesToMissingCircumference / 2) * R;
		double side1StartPointY = Units.sin(angleToOpening - angleBetweenRadiusesToMissingCircumference / 2) * R;
		// side1 endPoint:		
		double side1EndPointX = side1StartPointX - Units.cos(side1ToBaseDiameterAngle) * sideLength;
		double side1EndPointY = Units.cos(side1ToBaseDiameterAngle) * sideLength - side1StartPointY;

		Coordinates[] side1EndCoordinates = { new Coordinates(side1StartPointX, side1StartPointY),
				new Coordinates(side1EndPointX, side1EndPointY) };
		side1 = new Line(side1EndCoordinates, getCrossSection(), "Section Damage side1.");
		side1.setAngleWithBaseDiameter(side1ToBaseDiameterAngle);
		side1.setDistanceFromCrossSectionCenter(0);
		// side1 coordinates (intersection with inner and outer circles):
		Coordinates side1OuterCoordinates = (Utils.calculateChordWithCircleIntesectionPoints(side1,
				side1.getAngleWithBaseDiameter(), crossSection.getOutsideDiameter() - shellThickness * 2,
				new Coordinates(0, 0)))[0];
		Coordinates side1InnerCoordinates = (Utils.calculateChordWithCircleIntesectionPoints(side1,
				side1.getAngleWithBaseDiameter(), innerCircleDiameter, null))[0];
		side1.setEndCoordinates(new Coordinates[] { side1OuterCoordinates, side1InnerCoordinates });

		// side2 start point (intersection with cross section outline):
		double side2StartPointX = Units.cos(angleToOpening + angleBetweenRadiusesToMissingCircumference / 2) * R;
		double side2StartPointY = Units.sin(angleToOpening + angleBetweenRadiusesToMissingCircumference / 2) * R;
		double side2EndPointX = side2StartPointX - Units.cos(side2ToBaseDiameterAngle) * sideLength;
		double line2EndPointY = side2StartPointY - Units.cos(side2ToBaseDiameterAngle) * sideLength;

		// side2 end point = side1 end point = sides intersection
		Coordinates[] side2EndCoordinates = { new Coordinates(side2StartPointX, side2StartPointY),
				new Coordinates(side2EndPointX, line2EndPointY) };
		side2 = new Line(side2EndCoordinates, getCrossSection(), "Section Damage side2.");
		side2.setAngleWithBaseDiameter(side2ToBaseDiameterAngle);
		side2.setDistanceFromCrossSectionCenter(0);
		// side2 coordinates (intersection with inner and outer circles):
		Coordinates side2OuterCoordinates = (Utils.calculateChordWithCircleIntesectionPoints(side2,
				side2.getAngleWithBaseDiameter(), outerCircleDiameter, null))[0];
		Coordinates side2InnerCoordinates = (Utils.calculateChordWithCircleIntesectionPoints(side2,
				side2.getAngleWithBaseDiameter(), innerCircleDiameter, null))[0];
		side2.setEndCoordinates(new Coordinates[] { side2OuterCoordinates, side2InnerCoordinates });

		innerBoundary = new Line(new Coordinates[] { side1InnerCoordinates, side2InnerCoordinates }, crossSection,
				"Inner Boundary");
		outerBoundary = new Line(new Coordinates[] { side1OuterCoordinates, side2OuterCoordinates }, crossSection,
				"Outer Boundary");
	}

	@Override
	public void setPockets() {
		for (Line chord : crossSection.getChords()) {
			if (chord == null) {
				continue;
			}
			Coordinates[] intersectionCoordinates = calculateChordIntersection(chord);
			if (intersectionCoordinates == null) {
				continue;
			}
			chord.addGap(intersectionCoordinates, chord.getEndCoordinates()[0]);

		}
	}

	private Coordinates[] calculateChordIntersection(Line chord) {
		side1.setDistanceFromCrossSectionCenter(0);
		side2.setDistanceFromCrossSectionCenter(0);
		
		if (side1.doesIntersect(side1.calculateIntersectionWithAnotherChordByCoordinates(chord)) //OK
				&& side2.doesIntersect(side2.calculateIntersectionWithAnotherChordByCoordinates(chord))) {
			return new Coordinates[] { side1.calculateIntersectionWithAnotherChordByCoordinates(chord),
					side2.calculateIntersectionWithAnotherChordByCoordinates(chord) };
		} else if (side1.doesIntersect(side1.calculateIntersectionWithAnotherChordByCoordinates(chord)) && innerBoundary //OK
				.doesIntersect(innerBoundary.calculateIntersectionWithAnotherChordByCoordinates(chord))) {
			return new Coordinates[] { side1.calculateIntersectionWithAnotherChordByCoordinates(chord),
					(Utils.calculateChordWithCircleIntesectionPoints(chord, null, innerCircleDiameter, null))[0] };
		} else if (side1.doesIntersect(side1.calculateIntersectionWithAnotherChordByCoordinates(chord)) && outerBoundary
				.doesIntersect(outerBoundary.calculateIntersectionWithAnotherChordByCoordinates(chord))) {
			return new Coordinates[] { side1.calculateIntersectionWithAnotherChordByCoordinates(chord),
					(Utils.calculateChordWithCircleIntesectionPoints(chord, null, outerCircleDiameter, null))[0] };
		} else if (side2.doesIntersect(side2.calculateIntersectionWithAnotherChordByCoordinates(chord)) && innerBoundary
				.doesIntersect(innerBoundary.calculateIntersectionWithAnotherChordByCoordinates(chord))) {
			return new Coordinates[] { side2.calculateIntersectionWithAnotherChordByCoordinates(chord),
					(Utils.calculateChordWithCircleIntesectionPoints(chord, null, innerCircleDiameter, null))[0] };
		} else if (side2.doesIntersect(side2.calculateIntersectionWithAnotherChordByCoordinates(chord)) && outerBoundary
				.doesIntersect(outerBoundary.calculateIntersectionWithAnotherChordByCoordinates(chord))) {
			return new Coordinates[] { side2.calculateIntersectionWithAnotherChordByCoordinates(chord),
					(Utils.calculateChordWithCircleIntesectionPoints(chord, null, outerCircleDiameter, null))[0] };
		}	else if (innerBoundary.doesIntersect(innerBoundary.calculateIntersectionWithAnotherChord(chord)) &&
				outerBoundary.doesIntersect(outerBoundary.calculateIntersectionWithAnotherChord(chord)))	{
			return new Coordinates[] { (Utils.calculateChordWithCircleIntesectionPoints(chord, null, innerCircleDiameter, null))[0] ,
					(Utils.calculateChordWithCircleIntesectionPoints(chord, null, outerCircleDiameter, null))[0] };
		}
		return null;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("\n\tWedgeDamage: \n");
		sb.append("\n\t\tSide1: " + side1.toString());
		sb.append("\n\t\tSide2: " + side2.toString());
		sb.append("\n\t\tcenterLineToBaseDiameterAngle: " + angleToOpening);
		sb.append(", Missing Circumference: " + missingCircumference);
		sb.append(", depth: " + missingDepth);
		return sb.toString();
	}
}
