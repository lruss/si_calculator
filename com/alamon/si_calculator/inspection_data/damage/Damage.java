package com.alamon.si_calculator.inspection_data.damage;

import com.alamon.si_calculator.inspection_data.CrossSection;
import com.alamon.si_calculator.inspection_data.PoleData;

public abstract class Damage {	
	protected CrossSection crossSection;
	
	
	public Damage(CrossSection crossSection)	{
		this.crossSection = crossSection;			
	}	
	
	public Damage(PoleData poleData)	{
		crossSection = new CrossSection(poleData);		
	}

	public CrossSection getCrossSection() {
		return crossSection;
	}
	
	public void setCrossSection(CrossSection crossSection) {
		this.crossSection = crossSection;
	}
	
	public void addDamageToCrossSection(Damage damage)		{
		crossSection.addDamage(damage);
	}
	
	public abstract void setPockets();
}
