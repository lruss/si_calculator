package com.alamon.si_calculator.inspection_data.damage;

import com.alamon.si_calculator.inspection_data.CrossSection;

public class WoodPeckerCavityDamage {
	private static final String damageType = "Woodpecker Cavity Damage";
		
	/**
	 * This class is a combination of SectionDamage and HeartDamage objects
	 * All functionality is managed within those two objects so all we need to do is to call their constructors. 
	 * @param crossSection
	 * @param angleToOpening
	 * @param missingCircumference
	 * @param shellThickness
	 * @param nestDiameter
	 */
	@SuppressWarnings("unused")
	public WoodPeckerCavityDamage(CrossSection crossSection, double angleToOpening, double missingCircumference, double shellThickness, double nestDiameter) {		
		/**
		 * shellThickness for SectionDamage = 0 (it starts at the surface)
		 * missingDepth for SectionDamage = shellThickness for Heart Damage
		 */		
		System.out.println(
				"Damage of type [" + damageType + "] added to Cross Section. Two damage objects (Section and Heart) are added internally. Remaining strength for cross section "
						+ crossSection.getCrossSectionID() + " = " + crossSection.getRemainingStrength());
		SectionDamage sectionDamage = new SectionDamage(crossSection, angleToOpening, missingCircumference, shellThickness, 0);
		HeartDamage heartDamage = new HeartDamage(crossSection, shellThickness, shellThickness + nestDiameter, angleToOpening);
	}

	

	
}
