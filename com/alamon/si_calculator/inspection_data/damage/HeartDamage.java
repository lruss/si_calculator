package com.alamon.si_calculator.inspection_data.damage;

import com.alamon.si_calculator.inspection_data.CrossSection;
import com.alamon.si_calculator.math.geometry.Chord;
import com.alamon.si_calculator.math.geometry.Coordinates;
import com.alamon.si_calculator.math.utils.Units;

public class HeartDamage extends Damage {
	private double ringDepth;
	private double depth;
	private double radius;
	private Coordinates heartDamageCenterCoordinates;
	private static final String damageType = "Heart Damage";
	
	/**
	 * Angle between line connecting center of heartDamage and center of cross section.
	 * Need this to calculate coordinates of heartDamage center
	 */
	private double angleBetweenCenters;
	
	/**
	 * This constructor form is used for dcalc-like third parameter pocketDiameter instead of another form
	 * {@link #HeartDamage(CrossSection crossSection, double shellThickness, double depth, double angleBetweenCenters)}
	 * using depth as the third parameter
	 * The last parameter boolean asD_calc has no effect (any value can be passed) and is introduced to change signature thus 
	 * allowing constructor overloading
	 * TODO: consider using a factory-like method to return a new instance
	 * 
	 * @param crossSection
	 * @param shellThickness
	 * @param pocketDiameter
	 * @param angleBetweenCenters
	 * @param asD_calc
	 */
	 public HeartDamage(CrossSection crossSection, double shellThickness, double pocketDiameter, double angleBetweenCenters, boolean asD_calc) {
		super(crossSection);		
		double depth = shellThickness + pocketDiameter;
		new HeartDamage(crossSection, shellThickness, depth, angleBetweenCenters);
	}		
	

	/**
	 *  * This constructor form is used to pass depth as the third parameter instead of dcalc-like  pocketDiameter as the third parameter as
	 *  in this version {@link #HeartDamage(CrossSection crossSection, double shellThickness, double depth, double angleBetweenCenters, boolean asD_calc)}
	 * using depth ad the third parameter
	 * @param crossSection
	 * @param shellThickness
	 * @param depth
	 * @param angleBetweenCenters
	 */
	public HeartDamage(CrossSection crossSection, double shellThickness, double depth, double angleBetweenCenters) {
		super(crossSection);
		this.ringDepth = shellThickness;
		this.depth = depth;
		this.crossSection = crossSection;
		this.angleBetweenCenters = angleBetweenCenters;
		try {
		setCenterCoordinates();
		} catch (Exception e)	{
			System.out.println(e.getMessage());
		}
		setPockets();
		crossSection.setStructuralIntegrity();
		crossSection.setRemainingSectionModulus();
		System.out.println("Damage of type [" + damageType + "] added to Cross Section. Remaining strength for cross section " +
		crossSection.getCrossSectionID() + " = " + crossSection.getRemainingStrength());
	}

	private void setCenterCoordinates() throws Exception {
		double centerDistanceFromCrossSectionCenter = 0;
		if (depth + ringDepth > crossSection.getOutsideDiameter()) {
			throw new Exception(
					"ERROR: invalid parameters - sum of depth and shell thickness may not be  greater than cross section diameter.");
		}
		if (radius == 0 && depth == 0) {
			heartDamageCenterCoordinates = new Coordinates(0, 0);
			return;
		}
		radius = (depth - ringDepth) / 2;
		centerDistanceFromCrossSectionCenter = crossSection.getOutsideDiameter() / 2 + radius - depth;
		System.out.println("LR TEST centerDistanceFromCrossSectionCenter = " + centerDistanceFromCrossSectionCenter
				+ ", radius = " + radius);
		heartDamageCenterCoordinates = new Coordinates(
				Units.cos(angleBetweenCenters) * centerDistanceFromCrossSectionCenter,
				Units.sin(angleBetweenCenters) * centerDistanceFromCrossSectionCenter);
	}

	public void setPockets() {
		System.out.println("Pocket : " + " center coordinates: " + heartDamageCenterCoordinates.getX() + ", "
				+ heartDamageCenterCoordinates.getY());
		for (Chord chord : crossSection.getChords()) {
			if (chord == null) {
				continue;
			}
			Coordinates[] intersectionCoordinates = crossSection
					.calculateChordWithCircleIntesectionPoints(heartDamageCenterCoordinates, chord, radius * 2, null);
			if (Double.isNaN(intersectionCoordinates[0].getX()) || Double.isNaN(intersectionCoordinates[0].getY())
					|| Double.isNaN(intersectionCoordinates[1].getX())
					|| Double.isNaN(intersectionCoordinates[1].getY())) {
				continue;
			}
			chord.addGap(intersectionCoordinates, chord.getEndCoordinates()[0]);			
		}

	}

	public Coordinates getHeartDamageCenterCoordinates() {
		return heartDamageCenterCoordinates;
	}

	public double getRadius() {
		return radius;
	}
}
