package com.alamon.si_calculator.inspection_data.damage;

import com.alamon.si_calculator.inspection_data.CrossSection;
import com.alamon.si_calculator.math.geometry.Chord;
import com.alamon.si_calculator.math.geometry.Coordinates;
import com.alamon.si_calculator.math.geometry.Line;
import com.alamon.si_calculator.math.utils.Units;

public class WedgeDamage extends Damage {

	private double centerLineToBaseDiameterAngle;
	private double missingCircumference;
	private double depth;
	private static final String damageType = "Wedge Damage";
	private Line side1;
	private Line side2;
	private double sideLength;

	/**
	 * 
	 * @param crossSection
	 * @param centerLineToBaseDiameterAngle
	 * @param missingCircumference
	 * @param depth
	 */
	public WedgeDamage(CrossSection crossSection, double centerLineToBaseDiameterAngle, double missingCircumference,
			double depth) {
		super(crossSection);
		this.centerLineToBaseDiameterAngle = centerLineToBaseDiameterAngle;
		this.missingCircumference = missingCircumference;
		this.depth = depth;

		setSides();

		setPockets();
		crossSection.setStructuralIntegrity();
		crossSection.setRemainingSectionModulus();		
		super.addDamageToCrossSection(this);
		System.out.println(
				"Damage of type [" + damageType + "] added to Cross Section. Remaining strength for cross section "
						+ crossSection.getCrossSectionID() + " = " + crossSection.getRemainingStrength());
	}

	private void setSides() {
		double R = crossSection.getOutsideDiameter() / 2;
		// A3
		double angleBetweenRadiusesToMissingCircumference = (360 * missingCircumference)
				/ (Math.PI * crossSection.getOutsideDiameter());
		double BE = Units.sin(angleBetweenRadiusesToMissingCircumference / 2) * R;
		double AI = Units.cos(angleBetweenRadiusesToMissingCircumference / 2) * (R - depth);
		double IF = Units.sin(angleBetweenRadiusesToMissingCircumference / 2) * (R - depth);
		double IB = R - AI;
		sideLength = Math.sqrt(Math.pow(IF, 2) + Math.pow(IB, 2));
		// A4
		double angleBetweenSides = Units.aSin(BE / sideLength) * 2;
		System.out.println("LR TEST 	angleBetweenSides = " + angleBetweenSides);
		double side1ToBaseDiameterAngle = 180 + centerLineToBaseDiameterAngle - angleBetweenSides / 2;		
		double side2ToBaseDiameterAngle = angleBetweenSides - 180 + side1ToBaseDiameterAngle;

		System.out.println("LR TEST sidee1ToBaseDiameterAngle = " + side1ToBaseDiameterAngle
				+ ", side2ToBaseDiameterAngle = " + side2ToBaseDiameterAngle);
		// side1 start point (intersection with cross section outline):
		double side1StartPointX = Units
				.cos(centerLineToBaseDiameterAngle - angleBetweenRadiusesToMissingCircumference / 2) * R;
		double side1StartPointY = Units
				.sin(centerLineToBaseDiameterAngle - angleBetweenRadiusesToMissingCircumference / 2) * R;
		// side1 endPoint:
		double side1EndPointX = side1StartPointX - Units.cos(180 - side1ToBaseDiameterAngle) * sideLength;
		double side1EndPointY =  Units.cos(180 - side1ToBaseDiameterAngle) * sideLength - side1StartPointY;

		Coordinates[] side1EndCoordinates = { new Coordinates(side1StartPointX, side1StartPointY),
				new Coordinates(side1EndPointX, side1EndPointY) };
		side1 = new Line(side1EndCoordinates, getCrossSection(), "WedgeDamage side1.");
		side1.setAngleWithBaseDiameter(side1ToBaseDiameterAngle);

		// side2 start point (intersection with cross section outline):
		double line2StartPointX = Units
				.cos(centerLineToBaseDiameterAngle + angleBetweenRadiusesToMissingCircumference / 2) * R;
		double line2StartPointY = Units
				.sin(centerLineToBaseDiameterAngle + angleBetweenRadiusesToMissingCircumference / 2) * R;
		// side2 end point = side1 end point = sides intersection
		Coordinates[] side2EndCoordinates = { new Coordinates(line2StartPointX, line2StartPointY),
				new Coordinates(side1EndPointX, side1EndPointY) };
		side2 = new Line(side2EndCoordinates, getCrossSection(), "WedgeDamage side2.");		
		side2.setAngleWithBaseDiameter(side2ToBaseDiameterAngle);

		//System.out.println("LR TEST intersectionCoordinates = " + side1EndPointX + ", " + side1EndPointX);

	}

	@Override
	public void setPockets() {
		for (Chord chord : crossSection.getChords()) {
			if (chord == null) {
				continue;
			}
			Coordinates side1AndChordIntesection = side1.calculateIntersectionWithAnotherChordByCoordinates(chord);
			Coordinates side2AndChordIntesection = side2.calculateIntersectionWithAnotherChordByCoordinates(chord);
			if(side1AndChordIntesection == null && side2AndChordIntesection == null)	{
				continue;
			}
			boolean doesSide1AndChordIntesect = side1.doesIntersect(side1AndChordIntesection);
			boolean doesSide2AndChordIntesect = side2.doesIntersect(side2AndChordIntesection);

			// Scenario 1: chord intersects with both sides
			if (doesSide1AndChordIntesect && doesSide2AndChordIntesect) {
				chord.addGap(new Coordinates[] { side1AndChordIntesection, side2AndChordIntesection },
						chord.getEndCoordinates()[0]);
			}
			// Scenario 2: chord intersects with one side
			if (doesSide1AndChordIntesect && !doesSide2AndChordIntesect) {
				if (side1AndChordIntesection.compareTo(new Coordinates(0, 0)) > 0) {
					chord.addGap(new Coordinates[] { chord.getEndCoordinates()[0], side1AndChordIntesection },
							chord.getEndCoordinates()[0]);
				} else if (side1AndChordIntesection.compareTo(new Coordinates(0, 0)) < 0) {
					chord.addGap(new Coordinates[] { side1AndChordIntesection, chord.getEndCoordinates()[1] },
							chord.getEndCoordinates()[0]);
				}
			} else if (!doesSide1AndChordIntesect && doesSide2AndChordIntesect) {
				if (side2AndChordIntesection.compareTo(new Coordinates(0, 0)) > 0) {
					chord.addGap(new Coordinates[] { chord.getEndCoordinates()[0], side2AndChordIntesection },
							chord.getEndCoordinates()[0]);
				} else if (side2AndChordIntesection.compareTo(new Coordinates(0, 0)) < 0) {
					chord.addGap(new Coordinates[] { side2AndChordIntesection, chord.getEndCoordinates()[1] },
							chord.getEndCoordinates()[0]);
				}
			}			
		}
	}
	
	@Override
	public String toString()	{
		StringBuffer sb = new StringBuffer("\n\tWedgeDamage: \n");
		sb.append("\n\t\tSide1: " + side1.toString());
		sb.append("\n\t\tSide2: " + side2.toString());
		sb.append("\n\t\tcenterLineToBaseDiameterAngle: " + centerLineToBaseDiameterAngle);
		sb.append(", Missing Circumference: " + missingCircumference);
		sb.append(", depth: " + depth);
		return sb.toString();
	}
}
