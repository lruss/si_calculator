package com.alamon.si_calculator.inspection_data.damage;

import com.alamon.si_calculator.exceptions.InvalidEntryException;
import com.alamon.si_calculator.inspection_data.CrossSection;
import com.alamon.si_calculator.math.geometry.Coordinates;
import com.alamon.si_calculator.math.geometry.Line;
import com.alamon.si_calculator.math.utils.Units;
import com.alamon.si_calculator.math.utils.Utils;

public class HoleDamage extends Damage {

	private double angleToOpening;
	private double arcFromBaseToCenterLine;
	private Double depth;
	private static final String damageType = "Hole Damage";
	private Line side1;
	private Line side2;
	private Line bottomLine;
	private double holeDiameter;
	private static final String holeTooLargeMsg = "Hole diameter must be less than CrossSection circumference diameter.";
	private static final String holeD0Msg = "Hole diameter must be greater than 0.";
	private static final String arcTooLargeMsg = "Arc from base diameter to center line cannot be greater than ";
	private boolean isDebug;
	//private double sideLength;

	/**
	 * This constructor form is dumbed down to d-calc level.
	 * @param crossSection
	 * @param angleToOpening
	 * @param holeDiameter
	 * @param isDebug TODO
	 * @throws InvalidEntryException 
	 */
	public HoleDamage(CrossSection crossSection, double angleToOpening, double holeDiameter, boolean isDebug)
			throws InvalidEntryException {
		super(crossSection);
		this.angleToOpening = angleToOpening;
		this.holeDiameter = holeDiameter;
		new HoleDamage(crossSection, angleToOpening, 0, null, holeDiameter, isDebug);
	}

	/**
	 * This is an enhanced constructor version allowing to evaluate hole damage with centerline offset from cross section center
	 * @param crossSection
	 * @param angleToOpening
	 * @param arcFromBaseToCenterLine
	 * @param depth - if null, goes all the way through
	 * @param holeDiameter
	 * @param isDebug - if true, will print debug statements
	 * @throws InvalidEntryException
	 */
	public HoleDamage(CrossSection crossSection, double angleToOpening, double arcFromBaseToCenterLine, Double depth,
			double holeDiameter, boolean isDebug) throws InvalidEntryException {
		super(crossSection);
		this.isDebug = isDebug;
		if (holeDiameter >= crossSection.getOutsideDiameter()) {
			throw new InvalidEntryException(holeTooLargeMsg);
		}
		if (holeDiameter == 0) {
			throw new InvalidEntryException(holeD0Msg);
		}
		if (arcFromBaseToCenterLine > crossSection.getOutsideDiameter() * Math.PI / 2) {
			throw new InvalidEntryException(arcTooLargeMsg + crossSection.getOutsideDiameter() * Math.PI / 2);
		}
		this.angleToOpening = angleToOpening;
		this.arcFromBaseToCenterLine = arcFromBaseToCenterLine;
		this.depth = depth;
		this.holeDiameter = holeDiameter;

		setBoundaries();

		setPockets();
		crossSection.setStructuralIntegrity();
		crossSection.setRemainingSectionModulus();
		super.addDamageToCrossSection(this);
		if (isDebug) {
			System.out.println(
					"Damage of type [" + damageType + "] added to Cross Section. Remaining strength for cross section "
							+ crossSection.getCrossSectionID() + " = " + crossSection.getRemainingStrength());
		}
	}

	/**
	 * There are up to three sides defining hole damage geometry - 
	 * two sides and bottomLine if depth is defined. If depth is underfined (null), hole goes all the way through cross section
	 */
	private void setBoundaries() {
		double R = crossSection.getOutsideDiameter() / 2;
		double angleBetweenRadiusesAndArcEnds = Utils.calculateAngleByArcLengthAndDiameter(arcFromBaseToCenterLine,
				crossSection.getOutsideDiameter());
		double perpToCenterLine = Units.sin(angleToOpening) * R;
		double sinAngleToOpening = (angleToOpening == 0 ? 1 : Units.sin(angleToOpening));
		double centerLineOffset = 0;
		Line centerLine = null;
		if (angleToOpening == 0) {
			centerLineOffset = perpToCenterLine;
			centerLine = new Line(new Coordinates(0, 0), crossSection.getOutsideDiameter(), angleToOpening,
					centerLineOffset, crossSection);
		} else if (angleToOpening == 90) {
			Coordinates[] centerLineEndPoints = new Coordinates[] { new Coordinates(0, R), new Coordinates(0, -R) };
			centerLine = new Line(centerLineEndPoints, crossSection, "Hole Damage Centerline");
		} else {
			if (angleBetweenRadiusesAndArcEnds == 0) {
				centerLineOffset = 0;
			} else {
				centerLineOffset = Units.cos(180 - angleBetweenRadiusesAndArcEnds - angleToOpening) * R
						/ Units.sin(angleToOpening);
			}
		}

		if (angleToOpening == 90) {
			Coordinates[] centerLineEndPoints = centerLine.getEndCoordinates();
			Coordinates[] side1EndPoints = new Coordinates[] {
					new Coordinates(centerLineEndPoints[0].getX() + holeDiameter / 2,
							crossSection.getOutsideDiameter()),
					new Coordinates(centerLineEndPoints[0].getX() + holeDiameter / 2,
							-crossSection.getOutsideDiameter()) };
			side1 = new Line(side1EndPoints, crossSection, "Side1");
			Coordinates[] side2EndPoints = new Coordinates[] {
					new Coordinates(centerLineEndPoints[0].getX() - holeDiameter / 2,
							crossSection.getOutsideDiameter()),
					new Coordinates(centerLineEndPoints[0].getX() - holeDiameter / 2,
							-crossSection.getOutsideDiameter()) };
			side2 = new Line(side2EndPoints, crossSection, "Side2");
		} else if (angleToOpening == 0) {
			double side1Offset = ((centerLineOffset - (holeDiameter / 2) / sinAngleToOpening));
			side1 = new Line(new Coordinates(0, 0), crossSection.getOutsideDiameter(), angleToOpening, side1Offset,
					crossSection);
			double side2Offset = ((centerLineOffset + (holeDiameter / 2) / sinAngleToOpening));
			side2 = new Line(new Coordinates(0, 0), crossSection.getOutsideDiameter(), angleToOpening, side2Offset,
					crossSection);
		} else {
			double side1Offset = ((centerLineOffset - (holeDiameter / 2) / Units.cos(angleToOpening)));
			side1 = new Line(new Coordinates(0, 0), crossSection.getOutsideDiameter(), angleToOpening, side1Offset,
					crossSection);
			//double side2Offset = ((centerLineOffset + (holeDiameter / 2) * Units.sin(180-angleToOpening)));
			double side2Offset = centerLineOffset - side1Offset;
			side2 = new Line(new Coordinates(0, 0), crossSection.getOutsideDiameter(), angleToOpening, side2Offset,
					crossSection);
		}

		//Set side lengths to depth if depth is not null
		if (depth != null) {
			centerLine.setEndPointCoordinates(depth);
			double bottomLineOffsetFromCenter = ((centerLine.getLength() / 2 - depth) / Units.sin(angleToOpening + 90));

			bottomLine = new Line(new Coordinates(0, 0), crossSection.getOutsideDiameter(), angleToOpening + 90,
					bottomLineOffsetFromCenter, crossSection);

			Coordinates intersectionWithSide1 = side1.calculateIntersectionWithAnotherChord(bottomLine);
			Coordinates intersectionWithSide2 = side2.calculateIntersectionWithAnotherChord(bottomLine);
			bottomLine.setEndCoordinates(new Coordinates[] { intersectionWithSide1, intersectionWithSide2 });
			side1.getEndCoordinates()[1] = intersectionWithSide1;
			side2.getEndCoordinates()[1] = intersectionWithSide2;

		}

	}

	private Coordinates[] calculateChordIntersection(Line chord) {
		Coordinates chordIntersectionWithSide1 = null;
		if (angleToOpening == 90) {
			chordIntersectionWithSide1 = new Coordinates(side1.getEndCoordinates()[0].getX(),
					chord.getDistanceFromCrossSectionCenter());
		} else {
			chordIntersectionWithSide1 = side1.calculateIntersectionWithAnotherChordByCoordinates(chord);
		}
		Coordinates chordIntersectionWithSide2 = side2.calculateIntersectionWithAnotherChordByCoordinates(chord);
		Coordinates chordIntersectionWithBottomLine = (bottomLine == null ? null
				: bottomLine.calculateIntersectionWithAnotherChord(chord));

		if (side1.doesIntersect(chordIntersectionWithSide1) && side2.doesIntersect(chordIntersectionWithSide2)) {
			return new Coordinates[] { chordIntersectionWithSide1, chordIntersectionWithSide2 };
		} else if (chordIntersectionWithBottomLine != null && side1.doesIntersect(chordIntersectionWithSide1)
				&& bottomLine.doesIntersect(chordIntersectionWithBottomLine)) {
			return new Coordinates[] { chordIntersectionWithBottomLine, chordIntersectionWithSide1 };
		} else if (chordIntersectionWithBottomLine != null && side2.doesIntersect(chordIntersectionWithSide2)
				&& bottomLine.doesIntersect(chordIntersectionWithBottomLine)) {
			return new Coordinates[] { chordIntersectionWithBottomLine, chordIntersectionWithSide2 };
		} else if (chordIntersectionWithBottomLine != null && !side1.doesIntersect(chordIntersectionWithSide1)
				&& !side2.doesIntersect(chordIntersectionWithSide2)
				&& bottomLine.doesIntersect(chordIntersectionWithBottomLine)) {
			return new Coordinates[] { chord.getEndCoordinates()[0], chordIntersectionWithBottomLine };
		} else if (side1.doesIntersect(chordIntersectionWithSide1)) {
			return new Coordinates[] { chord.getEndCoordinates()[0], chordIntersectionWithSide1 };
		} else if (side2.doesIntersect(chordIntersectionWithSide2)) {
			return new Coordinates[] { chord.getEndCoordinates()[0], chordIntersectionWithSide2 };
		} else if (!side1.doesIntersect(chordIntersectionWithSide1) && !side1.doesIntersect(chordIntersectionWithSide1)
				&& chordIntersectionWithBottomLine == null
				&& ((chord.getDistanceFromCrossSectionCenter() < side1.getDistanceFromCrossSectionCenter()
						&& chord.getDistanceFromCrossSectionCenter() > side2.getDistanceFromCrossSectionCenter())
						|| (chord.getDistanceFromCrossSectionCenter() > side1.getDistanceFromCrossSectionCenter()
								&& chord.getDistanceFromCrossSectionCenter() < side2
										.getDistanceFromCrossSectionCenter()))) {
			return chord.getEndCoordinates();
		}
		return null;
	}

	@Override
	public void setPockets() {
		for (Line chord : crossSection.getChords()) {
			if (chord == null) {
				continue;
			}
			Coordinates[] intersectionCoordinates = calculateChordIntersection(chord);
			if (intersectionCoordinates == null) {
				continue;
			}
			if (isDebug) {
				System.out.println("Chord No. [" + chord.getChordNo() + "] intersection coordinates: " + intersectionCoordinates[0].getX() + ", "
						+ intersectionCoordinates[0].getY() + "; " + intersectionCoordinates[1].getX() + ", "
						+ intersectionCoordinates[1].getY());
			}
			if(chord.getChordNo() == -9)	{
				System.out.println("LR TEST PAUSE");
			}
			chord.addGap(intersectionCoordinates, chord.getEndCoordinates()[0]);
		}
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("\n\tWedgeDamage: \n");
		sb.append("\n\t\tSide1: " + side1.toString());
		sb.append("\n\t\tSide2: " + side2.toString());
		sb.append("\n\t\tcenterLineToBaseDiameterAngle: " + angleToOpening);
		sb.append(", Missing Circumference: " + arcFromBaseToCenterLine);
		sb.append(", depth: " + depth);
		return sb.toString();
	}
}
