package com.alamon.si_calculator.inspection_data.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alamon.resource_reader.json.LoadStaticResources;
import com.alamon.si_calculator.inspection_data.PoleData;
import com.alamon.si_calculator.math.utils.InputValidators;
import com.alamon.si_calculator.math.utils.Units;

/**
 * Created by lruss on 7/7/2017.
 */
public class CalculatorUtils {

    public static double calculateTaper(PoleData poleData) {
        if(poleData == null)    {
            System.out.println("PoleData object has not been initialized.");
            return -1;
        }
        if(poleData.getSpeciesClass() == null || poleData.getSpeciesClass().isEmpty() || poleData.getSpeciesSubClass() == null || poleData.getSpeciesSubClass().isEmpty())    {
            return calculateAverageTaper();
        } else  {
            Map<String, String> criteria = new HashMap<>();
            criteria.put("SPECIES CLASS", poleData.getSpeciesClass());
            criteria.put("SPECIES SUBCLASS", poleData.getSpeciesSubClass());
            try {
                LoadStaticResources.getFileContentsByFileName("woodSpeciesArr.json")
                        .getElementsByFieldNameAndValue(criteria).get(0).get("TAPER in-ft");
            } catch (Exception e) {
                System.out.println("No entries for \"SPECIES CLASS\" field with value \"" + poleData.getSpeciesClass() + "\" and \"SPECIES SUBCLASS\" with value \"" + poleData.getSpeciesSubClass() + "\" exist in woodSpeciesArr.json file." );
                double taper = calculateAverageTaper();
                System.out.println("Using calculated (average) taper value of " + taper);
                return taper;
            }
        }
        return 0;
    }

    private static double calculateAverageTaper()     {
        List<String> taperValues = null;
        try {
            taperValues = LoadStaticResources.getFileContentsByFileName("woodSpeciesArr.json")
                    .getAllValuesByFieldName("TAPER in-ft");
        } catch (Exception e) {}
        double ttl = 0;       
        int counter = 1;
       for(String taperStr : taperValues)  {
           if(taperStr.isEmpty())   {
               continue;
           }
           Double taperJsn = InputValidators.validateDoubleInput(taperStr);
           if(taperJsn == null || taperJsn == 0)  {
               continue;
           }
           ttl += taperJsn;
           counter++;
       }
        return Units.setDoublePrecision(ttl/counter);
    }

    public static double calculateTaperByDiameters(double D, double d, double distance)    {
        return ((D == d) ?  0 : (D -d) / distance);
    }

    /**
     * Calculate diameter of a pole based on baseDiameter (typically, at ground level), distance from baseDiameter cross section height,
     * height of cros section of interest, and taper. If distance value passed is positive, return result is smaller than baseDiameter,
     * if distance value passed is negative - greater. If taper or distance parameter is 0, return value equals baseDiameter.
     * @param baseDiameter
     * @param taper
     * @param distance
     * @return
     */
    public static double calculateDiameterByTaper(double baseDiameter, double taper, double distance)   {
        if(baseDiameter == 0 || distance == 0 || taper == 0)   {
            return baseDiameter;
        }
        return (distance > 0 ? (baseDiameter - taper) * distance : (taper - baseDiameter) * distance);
    }  
}
