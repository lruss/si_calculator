package com.alamon.si_calculator.inspection_data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lruss on 6/27/2017.
 */
public class PoleData   implements Cloneable {
    private String poleID;
    private double circumference;
    /*
    referenceDiameter usually intact outside diameter at ground level.
    Is used to calculate cross section diameter based on distance from ground and pole
     */
    private double referenceDiameter;
    private double length;
    private String speciesClass;
    private String speciesSubClass;
    private String poleClass;
    private double taper;
    //TODO: Check if damageLevel is a property of a pole or test/cross section
    private int damageLevel;
    private List<CrossSection> crossSections = new ArrayList<>();
    private double chordToBaseDiamAngle;

    /**
     * 
     * @param poleID
     * @param chordToBaseDiamAngle
     * @param referenceDiameter
     */    
    public PoleData(String poleID, double chordToBaseDiamAngle, double referenceDiameter)  {
        this.poleID = poleID;
        this.chordToBaseDiamAngle = chordToBaseDiamAngle;
        this.referenceDiameter = referenceDiameter;
    }
    
   public String getPoleID() {
        return poleID;
    }

    public double getChordToBaseDiamAngle() {
		return chordToBaseDiamAngle;
	}

	public void setPoleID(String poleID) {
        this.poleID = poleID;
    }

   public double getCircumference() {
        return circumference;
    }

    public void setCircumference(double circumference) {
        this.circumference = circumference;
        this.referenceDiameter = circumference / Math.PI;
    }

    public double getReferenceDiameter() {
        return referenceDiameter;
    }

    public void setReferenceDiameter(double referenceDiameter) {
        this.referenceDiameter = referenceDiameter;
        this.circumference = Math.PI * referenceDiameter;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public String getSpeciesClass() {
        return speciesClass;
    }

    public void setSpeciesClass(String speciesClass) {
        this.speciesClass = speciesClass;
    }

    public String getSpeciesSubClass() {
        return speciesSubClass;
    }

    public void setSpeciesSubClass(String speciesSubClass) {
        this.speciesSubClass = speciesSubClass;
    }

    public String getPoleClass() {
        return poleClass;
    }

    public void setPoleClass(String poleClass) {
        this.poleClass = poleClass;
    }

    public int getDamageLevel() {
        return damageLevel;
    }

    public void setDamageLevel(int damageLevel) {
        this.damageLevel = damageLevel;
    }

    public double getTaper() {
        return taper;
    }

    public void setTaper(double taper) {
        this.taper = taper;
    }

    /**
     * Returns a collection (List) of cross section tests for this pole
     * @return
     */
    public List<CrossSection> getCorssSections() {
        return crossSections;
    }

    /**
     * Adds passed corssSectionTestData object to a collection (List) of cross section tests for this pole and returns it
     * @param crossSection     
     */
    public void addCrossSectionTestData(CrossSection crossSection) {
        this.crossSections.add(crossSection);     
    }
    
    public List<CrossSection> getCrossSections() {
    	return crossSections;
    }
    
    @Override
    public PoleData clone()	{
    	return new PoleData(poleID, chordToBaseDiamAngle, referenceDiameter);	    	
    }

    @Override
    public String toString()    {
        StringBuilder sb = new StringBuilder("\nPoleID: " + poleID + "\n");
        sb.append("\tCircumference: " + circumference + " in\n");
        sb.append("\tDiameter: " + referenceDiameter + " in\n");
        sb.append("\tLength: " + length + " ft\n");
        sb.append("\tTaper: " + taper + " in-ft\n");

        sb.append("\tSpecies Class: " + speciesClass + "\n");
        sb.append("\tSpecies Sub Class: " + speciesSubClass + "\n");
        sb.append("\tPole class: " + poleClass + "\n");
        sb.append("\tDamage Level: " + damageLevel + "\n");
        for(CrossSection crossSecion : crossSections)   {
            sb.append("\t" + crossSecion.toString() + "\n");
        }
        return sb.toString();
    }
}
