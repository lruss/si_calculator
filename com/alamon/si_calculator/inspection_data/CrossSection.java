package com.alamon.si_calculator.inspection_data;

import java.util.ArrayList;
import java.util.List;

import com.alamon.resource_reader.json.LoadStaticResources;
import com.alamon.si_calculator.inspection_data.damage.Damage;
import com.alamon.si_calculator.math.geometry.Chord;
import com.alamon.si_calculator.math.geometry.Coordinates;
import com.alamon.si_calculator.math.geometry.Line;
import com.alamon.si_calculator.math.utils.InputValidators;
import com.alamon.si_calculator.math.utils.Units;

/**
 * Created by lruss on 6/27/2017.
 */
public class CrossSection implements Cloneable {

	private double distanceFromGround;
	private int damageLevel;
	private double outsideDiameter;
	private String crossSectionID;
	private String poleID;
	private List<Damage> damages = new ArrayList<>();
	private final int noOfChords = (LoadStaticResources.getFieldValue("varConstantsArr.json", "name", "noOfChords",
			"value") == null ? 10
					: InputValidators.validateIntInput(
							LoadStaticResources.getFieldValue("varConstantsArr.json", "name", "noOfChords", "value")));
	private double remainingStrength;
	private double originalSectionModulus;
	private double remainingSectionModulus;
	private double originalArea;
	private PoleData poleData;

	private double distanceBetweenCords = 0;
	List<Line> chords = new ArrayList<>();
	/**
	 * /**
	 * Angle between base diameter (chordNo 0) and chords perpendicular to force direction.
	 * Force direction angle = 90 deg + chord-to-base diameter angle
	 * 
	 */
	private double chordToBaseDiamAngle;

	public List<Line> getChords() {
		return chords;
	}

	/**
	 * Initialize cords collection - no need to pass it to this object since CrossSection argument to the only constructor
	 * has everything needed to create Chord objects. 
	 * Positive number signifies compression side of the cross section, negative - tension
	 * @param chords
	 */
	private void setChords() {
		// Chord no = 0 if diameter of crossSection, and it's not taken into
		// consideration because it has no affect on structural integrity
		distanceBetweenCords = outsideDiameter / noOfChords;
		for (int chordNo = 1; chordNo < noOfChords / 2; chordNo++) {
			Line chord = new Line(chordNo, distanceBetweenCords, this);
			chord.setLength();
			calculateChordWithCircleIntesectionPoints(null, chord, 0, null);
			chords.add(chord);
		}
		for (int chordNo = 1; chordNo < noOfChords / 2; chordNo++) {
			Line chord = new Line(-chordNo, distanceBetweenCords, this);
			chord.setLength();
			calculateChordWithCircleIntesectionPoints(null, chord, 0, null);
			chords.add(chord);
		}
	}

	/**
	 * 
	 * @param poleID
	 * @param outsideDiameter
	 * @param chordToBaseDiamAngle
	 */
	public void setCrossSection(PoleData poleData, double outsideDiameter) {
		this.poleID = poleData.getPoleID();
		this.outsideDiameter = outsideDiameter;
		this.distanceBetweenCords = outsideDiameter / noOfChords;
		this.chordToBaseDiamAngle = poleData.getChordToBaseDiamAngle();
		setChords();
		this.originalArea = Math.PI * (Math.pow(outsideDiameter, 2) / 4);
		this.originalSectionModulus = (Math.PI * Math.pow(getOutsideDiameter(), 3)) / 32;
	}

	public CrossSection(PoleData poleData) {		
		this.poleData = poleData;
		// TODO: replace pole diameter with cross section diameter
		setCrossSection(poleData, poleData.getReferenceDiameter());
		crossSectionID = poleData.getPoleID() + "_CC-" + +poleData.getCorssSections().size();
		poleData.addCrossSectionTestData(this);
	}

	public String getCrossSectionID() {
		return crossSectionID;
	}

	public void setCrossSectionID(String crossSectionID) {
		this.crossSectionID = crossSectionID;
	}

	public String getPoleID() {
		return poleID;
	}

	public void setPoleID(String poleID) {
		this.poleID = poleID;
	}

	public String getScrossSectionID() {
		return crossSectionID;
	}

	public void setScrossSectionId(String scrossSectionId) {
		this.crossSectionID = scrossSectionId;
	}

	public double getDistanceFromGround() {
		return distanceFromGround;
	}

	public void setDistanceFromGround(double distanceFromGround) {
		this.distanceFromGround = distanceFromGround;
	}

	public int getDamageLevel() {
		return damageLevel;
	}

	public void setDamageLevel(int level) {
		this.damageLevel = level;
	}

	public double getOutsideDiameter() {
		return outsideDiameter;
	}

	public void setOutsideDiameter(double intactOutsideDiameter) {
		this.outsideDiameter = intactOutsideDiameter;
	}

	public List<Damage> getDamages() {
		return damages;
	}

	public void addDamage(Damage damage) {
		this.damages.add(damage);
	}

	/**
	 * Angle between base diameter (chordNo 0) and chords perpendicular to force direction.
	 * Force direction angle = 90 deg + chord-to-base diameter angle
	 * @return
	 */
	public double getChordToBaseDiamAngle() {
		return chordToBaseDiamAngle;
	}

	public int getNoOfChords() {
		return noOfChords;
	}

	public PoleData getPoleData() {
		return this.poleData;
	}

	public void setChordToBaseDiamAngle(double forceDirectionAngle) {
		this.chordToBaseDiamAngle = forceDirectionAngle;
	}

	/**
	 * This is what we are here for
	 */
	public void setStructuralIntegrity() {
		double ttlIntact = 0.0;
		double ttlRemainingIntact = 0.0;
		double coefficient = 0.0;
		for (Chord chord : getChords()) {
			coefficient = Math.pow(chord.getDistanceFromCrossSectionCenter(), 2);
			ttlRemainingIntact += coefficient * (((chord.getLength() - chord.getTotalGapsLength())));
			ttlIntact += coefficient * chord.getLength();
		}
		remainingStrength = ttlRemainingIntact / ttlIntact;
	}

	public double getRemainingStrength() {
		return remainingStrength;
	}

	@Override
	public CrossSection clone() {
		return new CrossSection(this.poleData.clone());
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("\nCross Section ID: " + crossSectionID);
		sb.append("\tOriginal Area \t: " + originalArea);
		sb.append("\n\tDistance from ground \t: " + distanceFromGround + "(ft)\n");
		sb.append("\tdamage Level \t:" + damageLevel + "\n");
		sb.append("\tRemaining Strength \t: " + remainingStrength);
		sb.append("\tChords:\n");
		for (Chord chord : chords) {
			sb.append(chord.toString());
		}
		for (Damage damage : damages) {
			sb.append(damage.toString());
		}
		sb.append("\ttotalRelativeIntegrity \t:" + remainingStrength * 100 + "(%)\n");
		return sb.toString();
	}

	/**
	 * Find intersection points of a chord and circle;
	 * @param circleCenterCoordinates
	 * 		If null: calculate end points and full chord length (distance between 
	 * 				 intersection of a chord and cross section outline and set end points
	 * 				 and total chord length values to the Chord instance
	 * 		If not null: calculate distance between intersection points of a chord and heart 
	 * 				damage outline and set a new gap object to the Chord instance
	 * @param chord	
	 * @param circleDiameter - if circleCenterCoordinates is null (cross section chords are calculated),
	 * 						   this value is irrelevant, and cross section diameter is used.	
	 * @param overrideChordAngle - if null, will use value from pole data object. Otherwise, will override
	 * pole data value. This is useful when need to create an instance for special cases for example, when applied
	 * force is not perpendicular to base diameter.
	 */
	public Coordinates[] calculateChordWithCircleIntesectionPoints(Coordinates circleCenterCoordinates, Chord chord,
			double circleDiameter, Double overrideChordAngle) {
		double angle = (overrideChordAngle == null ? getChordToBaseDiamAngle() : overrideChordAngle);
		double offsetFromXaxis = chord.getDistanceFromCrossSectionCenter() / Units.cos(angle);
		// Calculate end points with a cross section outline
		double circleCenX = 0;
		double circleCenY = 0;
		if (circleCenterCoordinates == null || circleDiameter == 0) {
			circleDiameter = getOutsideDiameter();

		}
		// Calculate end points with a heart damage outline
		if (circleCenterCoordinates != null) {
			circleCenX = circleCenterCoordinates.getX();
			circleCenY = circleCenterCoordinates.getY();
			//angle = getChordToBaseDiamAngle();

		}

		double a = 1 + Math.pow(Units.tan(angle), 2);
		double b = -2 * circleCenX + 2 * Units.tan(angle) * offsetFromXaxis - 2 * Units.tan(angle) * circleCenY;
		double c = Math.pow(circleCenX, 2) + Math.pow(offsetFromXaxis, 2) - 2 * offsetFromXaxis * circleCenY
				+ Math.pow(circleCenY, 2) - Math.pow((circleDiameter / 2), 2);

		double interX1 = (-b + Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / (2 * a);
		double interX2 = (-b - Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / (2 * a);
		double interY1 = Units.tan(angle) * interX1 + offsetFromXaxis;
		double interY2 = Units.tan(angle) * interX2 + offsetFromXaxis;

		// Signifies calculation for cross section
		if (circleCenterCoordinates == null) {
			chord.setEndCoordinates(interX1, interY1, interX2, interY2);
			return chord.getEndCoordinates();
		}
		return new Coordinates[] { new Coordinates(interX1, interY1), new Coordinates(interX2, interY2) };
	}

	/**
	 * Section modulus of an intact (no damage) cross section
	 * @return
	 */
	public double getIntactSectionModulus() {
		return originalSectionModulus;
	}

	public double getRemainingSectionModulus() {
		return remainingSectionModulus;
	}

	public void setRemainingSectionModulus() {
		this.remainingSectionModulus = originalSectionModulus * remainingStrength;
	}
}