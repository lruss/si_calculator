package com.alamon.si_calculator.exceptions;

import java.util.ArrayList;
import java.util.List;

public class ErrorMessages {
	private static List<String> errorMessages = new ArrayList<>();

	public static List<String> getErrorMessages() {
		return errorMessages;
	}

	/**
	 * Add an error message to errorMessages list
	 * @param message
	 * @param reset - if true, clear messages before adding a new one
	 */
	public static void addErrorMessages(String message, boolean reset) {
		if(reset)	{
			errorMessages.clear();
		}
		errorMessages.add(message);
	}
	
	public static void clearMessages()	{
		errorMessages.clear();
	}
	
	public static String printMessages	()	{
		StringBuffer sb = new StringBuffer();
		for(String message : errorMessages)	{
			sb.append("\n" + message);
		}
		return sb.toString();
	}
	
	
	
}
