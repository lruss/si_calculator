package com.alamon.si_calculator.exceptions;

public class InvalidEntryException extends Exception {
	String output;
	private static final long serialVersionUID = 3912980777551600484L;

	public InvalidEntryException(String message)	{
		super(message);
	}
}
