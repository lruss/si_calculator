package com.alamon.si_calculator.math.geometry;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.alamon.si_calculator.inspection_data.CrossSection;
import com.alamon.si_calculator.inspection_data.PoleData;
import com.alamon.si_calculator.math.utils.ChordBreakComparator;
import com.alamon.si_calculator.math.utils.Units;

public class Chord implements Cloneable {
	double length;
	/**
	 * Chord number counted from base cross section diameter (cross section diameter is chord No. 0)
	 */
	private int chordNo;
	private double distanceFromCrossSectionCenter;
	private Coordinates[] endCoordinates = new Coordinates[] { new Coordinates(), new Coordinates() };
	private CrossSection crossSection;
	private double remainingIntact;
	private double intact;
	/**
	 * A collection of Double valued representing distances between the starting point of the cord and 
	 */
	private List<Pocket> gaps = new ArrayList<>();
	private double totalGapsLength;

	/**
	 * This constructor is used to instantiate chords when CrossSection object is created. ChordNo and 
	 * distanceBetweenCords parameters are used to calculate offset from circle center.
	 * It can also be used to create a generic stand-alone Chord/Line instance when offset from
	 * circle center is known. To do that, set chordNo parameter to 1 and distanceBetweenCords to offset.
	 * Custom constructor
	 * @param chordNo
	 * @param distanceBetweenCords
	 */
	public Chord(int chordNo, double distanceBetweenCords, CrossSection crossSection) {		
		this.chordNo = chordNo;
		this.crossSection= crossSection;
		//distanceFromCrossSectionCenter = Math.abs(chordNo * distanceBetweenCords);
		//TODO: include chord-to-base diameter angle in the following expression
		distanceFromCrossSectionCenter = chordNo *distanceBetweenCords;		
		setLength();
	}

	public void addGap(Coordinates[] damageIntersectionCoordinates, Coordinates chordStartCoordinates) {
		if (damageIntersectionCoordinates == null || Double.isNaN(damageIntersectionCoordinates[0].getX())
				|| Double.isNaN(damageIntersectionCoordinates[0].getY())
				|| Double.isNaN(damageIntersectionCoordinates[1].getX())
				|| Double.isNaN(damageIntersectionCoordinates[1].getY())) {
			return;
		}
		Pocket gap = new Pocket(damageIntersectionCoordinates, chordStartCoordinates, gaps.size());
		if (!Double.isNaN(gap.getStartPoint()) && !Double.isNaN(gap.getEndPoint()) && gap.getLength() != 0
				&& !Double.isNaN(gap.getLength())) {
			gaps.add(gap);
			superimposeGaps();
			setTotalGapsLength();		
			//System.out.println("LR TEST total chord gaps length for chord " + chordNo + " =  " + totalGapsLength);
		}
	}

	private void setTotalGapsLength() {
		this.totalGapsLength = 0;
		for (Pocket gap : gaps) {
			this.totalGapsLength += gap.getLength();
		}
	}

	public double getTotalGapsLength()	{
		return totalGapsLength;
	}
	
	/**
	 * Method used to calculate generic chord lenght
	 * @param distanceFromCrossSectionCenterOverride - if null, use this.distanceFromCrossSectionCenter based on chord number and distance between chords
	 * @param outsideDiameterOverride - if null, use cross section diameter
	 * @return
	 */
	public double setLength(Double distanceFromCrossSectionCenterOverride, Double outsideDiameterOverride) {
		double distance = (distanceFromCrossSectionCenterOverride == null ? this.distanceFromCrossSectionCenter : distanceFromCrossSectionCenterOverride);
		double diameter = (outsideDiameterOverride == null ? crossSection.getOutsideDiameter() : outsideDiameterOverride);
		return Math.sqrt(
				Math.pow((diameter / 2), 2) - Math.abs(Math.pow(distance, 2))) * 2;	
	}
	
	/**
	 * Calculate chord length (by distance between points of intersection of chord and outside cross section diameter using class variables)
	 * based on distanceFromCrossSectionCenter.
	 * Used to initiate chords when cross section object is created
	 * @return result
	 */
	public double setLength() {
		this.length = Math.sqrt(
				Math.pow((crossSection.getOutsideDiameter() / 2), 2) - Math.abs(Math.pow(distanceFromCrossSectionCenter, 2))) * 2;
		return length;
	}

	public List<Pocket> getGaps() {
		return gaps;
	}
	
	public void superimposeGaps() {
		Collections.sort(gaps, new ChordBreakComparator());
		if (chordNo == 2) {
			System.out.println("");
		}
		if (gaps.size() < 2) {
			return;
		}
		for (int i = 0; i < gaps.size(); i++) {
			Pocket current = gaps.get(i);
			Pocket next = null;
			try {
				next = gaps.get(i + 1);
			} catch (IndexOutOfBoundsException e) {
				break;
			}
			if (next.getStartPoint() >= current.getEndPoint()) {
				continue;
			} else if (next.getStartPoint() == current.getEndPoint()) {
				current.setEndPoint(next.getEndPoint());
				gaps.remove(i + 1);
			} else if (next.getEndPoint() <= current.getEndPoint() && next.getStartPoint() >= current.getStartPoint()) {
				gaps.remove(i + 1);
			} else if (next.getEndPoint() >= current.getEndPoint() && current.getStartPoint() <= next.getStartPoint()) {
				current.setEndPoint(next.getEndPoint());
				gaps.remove(i + 1);
				current.setLength();
			} else if (next.getEndPoint() >= current.getEndPoint() && next.getStartPoint() <= current.getStartPoint()) {
				current.setEndPoint(next.getEndPoint());
				current.setStartPoint(next.getStartPoint());
				current.setLength();
			} else if (next.getEndPoint() <= current.getEndPoint() && next.getStartPoint() <= current.getStartPoint()) {
				current.setStartPoint(next.getStartPoint());
				gaps.remove(i + 1);
				current.setLength();
			} else if (next.getEndPoint() >= current.getEndPoint() && next.getStartPoint() >= current.getStartPoint()) {
				current.setEndPoint(next.getEndPoint());
				gaps.remove(i + 1);
				current.setLength();
			}			
		}
	}

	public void setDistanceFromCrossSectionCenter(double distanceFromCrossSectionCenter) {
		//this.distanceFromCrossSectionCenter = Math.abs(chordNo * distanceBetweenCords);
		this.distanceFromCrossSectionCenter = distanceFromCrossSectionCenter;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("\n===================\n\tChord No. " + chordNo + ":\n");
		sb.append("length: " + length + ", distanceFromCrossSectionCenter: " + distanceFromCrossSectionCenter + "\n");
		sb.append("Start coordinates: " + endCoordinates[0].getX() + ", " + endCoordinates[0].getY()
				+ "; End coordinates: " + endCoordinates[1].getX() + ", " + endCoordinates[1].getY());
		for (Pocket gap : gaps) {
			sb.append("\n\tPocket - " + gap.toString());
		}
		sb.append(", Integrity: chord remaining intact = " + remainingIntact + ", intact = " + intact);
		return sb.toString();
	}

	public double getDistanceFromCrossSectionCenter() {
		return distanceFromCrossSectionCenter;
	}

	public double calculateChordLengthByEndCoordinates() {
		return Math.sqrt(Math.pow(endCoordinates[0].getX() - endCoordinates[1].getX(), 2)
				+ Math.pow(endCoordinates[0].getY() - endCoordinates[1].getY(), 2));
	}	

	public double getLength() {
		return length;
	}

	public int getChordNo() {
		return chordNo;
	}

	public Coordinates[] getEndCoordinates() {
		return endCoordinates;
	}
	
	public double getRemainingIntact() {
		return remainingIntact;
	}

	public double getIntact() {
		return intact;
	}	
	
	public CrossSection getCrossSection()	{
		return crossSection;
	}
	
	@Override
	public Chord clone()	{		 
		return new Chord(this.chordNo, this.distanceFromCrossSectionCenter, this.crossSection.clone());
	}
	
	/**
	 * 
	 * @param endCoordinates
	 */
	public void setEndCoordinates(Coordinates[] endCoordinates) {
		this.endCoordinates = endCoordinates;
		this.length = calculateChordLengthByEndCoordinates();
	}

	public void setEndCoordinates(double x1, double y1, double x2, double y2) {
		endCoordinates[0].setX(x1);
		endCoordinates[0].setY(y1);
		endCoordinates[1].setX(x2);
		endCoordinates[1].setY(y2);
	}
}
