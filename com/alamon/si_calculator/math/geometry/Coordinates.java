package com.alamon.si_calculator.math.geometry;

public class Coordinates  implements Comparable<Coordinates>{
	private double x, y;
	
	public Coordinates(double x, double y)	{
		this.x = x;
		this.y = y;
	}
	
	public Coordinates()	{}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}	
	
	@Override
	public int compareTo(Coordinates arg1) {
		return new Double((getX() - arg1.getX())).intValue();
	}
	
}
