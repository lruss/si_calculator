package com.alamon.si_calculator.math.geometry;

import com.alamon.si_calculator.math.utils.Utils;

public class Pocket {
	private int pocketId;
	/**
	 * Distance from start point of the chord and start of the break
	 */
	private double startPoint;
	/**
	 * Distance from start point of the chord and end of the break
	 */
	private double endPoint;
	private double length;
	private Coordinates[] damageIntersectionCoordinates;

//	public Pocket(double startPoint, double endPoint) {
//		this.startPoint = startPoint;
//		this.endPoint = endPoint;
//		orderStartEndPoints(startPoint, endPoint);
//
//	}

	/**
	 * 
	 * @param damageIntersectionCoordinates
	 * @param chordStartCoordinates
	 * @param pocketId
	 */
	public Pocket(Coordinates[] damageIntersectionCoordinates, Coordinates chordStartCoordinates, int pocketId) {
		this.damageIntersectionCoordinates = damageIntersectionCoordinates;
		this.pocketId += pocketId;		
		this.startPoint = Utils.getDistanceBetweenPointsByCoordinates(chordStartCoordinates,
				damageIntersectionCoordinates[0]);
		this.endPoint = Utils.getDistanceBetweenPointsByCoordinates(chordStartCoordinates,
				damageIntersectionCoordinates[1]);
		this.length = Math.abs(endPoint - startPoint);//Utils.getDistanceBetweenPointsByCoordinates(damageIntersectionCoordinates);
//		System.out.println("Pocket added, length " + length + ", distance between chord start and pocket start: " + startPoint +
//				", distance between chord start and pocket end: " + endPoint);
		orderStartEndPoints(startPoint, endPoint);

	}

	public double getStartPoint() {
		return startPoint;
	}

	public void setStartPoint(double startPoint) {
		this.startPoint = startPoint;
	}

	public double getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(double endPoint) {
		this.endPoint = endPoint;
	}

	private void orderStartEndPoints(double startPoint, double endPoint) {
		if (startPoint < endPoint) {
			double tmp = startPoint;
			startPoint = endPoint;
			endPoint = tmp;
		}
	}

	public double getLength () {
		return this.length;
	}
	
	public int getPocketId	()	{
		return pocketId;
	}
	
	public void setLength()	{
		//this.length = endPoint - startPoint;//Utils.getDistanceBetweenPointsByCoordinates(damageIntersectionCoordinates);
		this.length = Utils.getDistanceBetweenPointsByCoordinates(damageIntersectionCoordinates);
	}
	
	@Override
	public String toString() {
		return "PocketId: " + pocketId + ", damageIntersectionCoordinates: " + 
				damageIntersectionCoordinates[0].getX() + ", " + damageIntersectionCoordinates[0].getY() +
				"; " + damageIntersectionCoordinates[1].getX() + ", " + damageIntersectionCoordinates[1].getY() + "\n\t\t Start: " + startPoint + ", end: " + endPoint + ", length : " + length;
	}

}