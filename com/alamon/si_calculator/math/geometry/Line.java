package com.alamon.si_calculator.math.geometry;

import com.alamon.si_calculator.inspection_data.CrossSection;
import com.alamon.si_calculator.math.utils.Units;
import com.alamon.si_calculator.math.utils.Utils;

public class Line extends Chord {
	private double angleWithBaseDiameter;
	private String lineId;

	/**
	 * 
	 * @param endPoints
	 * @param crossSection
	 * @param lineId
	 */
	public Line(Coordinates[] endPoints, CrossSection crossSection, String lineId) {
		super(1, 0, crossSection);
		this.lineId = lineId;
		setEndCoordinates(endPoints);

		//Order start and end by x coordinates
		if (endPoints[0].compareTo(endPoints[1]) < 0) {
			Coordinates start = endPoints[1];
			endPoints[1] = endPoints[0];
			endPoints[0] = start;
		}
		this.angleWithBaseDiameter = Utils.getLineAngleByEndPoints(getEndCoordinates());
		setDistanceFromCrossSectionCenter();
	}
	
	/**
	 * 
	 * @param offsetFromCenter
	 * @param crossSection
	 */
	public Line(double offsetFromCenter, CrossSection crossSection)	{
		super(1, offsetFromCenter, crossSection); 
	}
	
	public Line(int chordNo, double distanceBetweenChords, CrossSection crossSection)	{
		super(chordNo, distanceBetweenChords, crossSection); 
	}
	
	/**
	 * Create a chord 
	 * @param circleCenterCoordinates
	 * @param circleDiameter
	 * @param angleToBaseDiameter
	 * @param offsetFromCenter
	 * @param crossSection
	 */
	public Line(Coordinates circleCenterCoordinates, double circleDiameter, Double angleToBaseDiameter,
			double offsetFromCenter, CrossSection crossSection) {
		super(1, offsetFromCenter, crossSection); 				
		this.angleWithBaseDiameter = angleToBaseDiameter.doubleValue();
		this.setDistanceFromCrossSectionCenter(offsetFromCenter);
//		Chord chord = new Chord(1, offsetFromCenter, this);
		//length = setLength(offsetFromCenter, circleDiameter);
		Coordinates[] intersectionWithCircumference = Utils.calculateChordWithCircleIntesectionPoints(this, angleToBaseDiameter, circleDiameter, circleCenterCoordinates) ;
		setEndCoordinates(intersectionWithCircumference);					
		//length = setLength(offsetFromCenter, circleDiameter);
		//setEndCoordinates(length);		
	}

	public void setAngleWithBaseDiameter(double angle) {
		this.angleWithBaseDiameter = angle;
	}

	public double getAngleWithBaseDiameter() {
		return this.angleWithBaseDiameter;
	}

	/**
	 * Set coordinates of line end based on start coordinates and length
	 * @param length
	 */
	public void setEndPointCoordinates(double length) {
		// First chord end (start of) coordinate remains the same - intersection  with outer circle/circumference
		// need to update the end coordinate based on provided length and angle with base diameter
		getEndCoordinates()[1].setX(
				getEndCoordinates()[0].getX() - (Units.cos(angleWithBaseDiameter) * length));
		getEndCoordinates()[1].setY(
				getEndCoordinates()[0].getY() - (Units.sin(angleWithBaseDiameter) * length));
		//Check if length is 0 = there is no such thing as angle between two lines when one of them has no length so
		//the following statement will throw a division by 0 exception. The previous two statements will set end 
		//coordinates values to those of start coordinates.
		if (length > 0) {			
			angleWithBaseDiameter = Units.aSin((getEndCoordinates()[0].getY() - getEndCoordinates()[1].getY())
					/ (getEndCoordinates()[0].getX() - getEndCoordinates()[1].getX()));
		}		
	}

	public Coordinates calculateIntersectionWithAnotherChord1(Chord line1) {
		angleWithBaseDiameter = Utils.getLineAngleByEndPoints(getEndCoordinates());

		double line1AngleBackup = line1.getCrossSection().getChordToBaseDiamAngle();
		line1.getCrossSection().setChordToBaseDiamAngle(Utils.getLineAngleByEndPoints(line1.getEndCoordinates()));
		double x = (line1.getDistanceFromCrossSectionCenter() - getDistanceFromCrossSectionCenter())
				/ (Units.tan(angleWithBaseDiameter) - Units.tan(line1.getCrossSection().getChordToBaseDiamAngle()));
		double y = Units.tan(angleWithBaseDiameter) * x + this.getDistanceFromCrossSectionCenter();
		line1.getCrossSection().setChordToBaseDiamAngle(line1AngleBackup);
		return new Coordinates(x, y);
	}
	
	public Coordinates calculateIntersectionWithAnotherChord(Line line1) {
		angleWithBaseDiameter = Utils.getLineAngleByEndPoints(getEndCoordinates());		
		//line1.getCrossSection().setChordToBaseDiamAngle(Utils.getLineAngleByEndPoints(line1.getEndCoordinates()));
		double x = (line1.getDistanceFromCrossSectionCenter() - getDistanceFromCrossSectionCenter())
				/ (Units.tan(angleWithBaseDiameter) - Units.tan(line1.getAngleWithBaseDiameter()));
		double y = Units.tan(angleWithBaseDiameter) * x + this.getDistanceFromCrossSectionCenter();
		
		return new Coordinates(x, y);
	}

	public Coordinates calculateIntersectionWithAnotherChordByCoordinates(Chord line1) {
		double x1 = getEndCoordinates()[0].getX();
		double y1 = getEndCoordinates()[0].getY();
		double x2 = getEndCoordinates()[1].getX();
		double y2 = getEndCoordinates()[1].getY();
		double x3 = line1.getEndCoordinates()[0].getX();
		double y3 = line1.getEndCoordinates()[0].getY();
		double x4 = line1.getEndCoordinates()[1].getX();
		double y4 = line1.getEndCoordinates()[1].getY();
		double u_a = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / ((y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1));
		double u_b = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / ((y4 -	y3) * (x2 - x1) - (x4 - x3) * (y2 - y1));

		double X = x1 + u_a * (x2 - x1);
		double Y = y1 + u_a * (y2 - y1);
		return new Coordinates(X, Y);
	}

	public boolean doesIntersect(Coordinates intersection) {
		return Math.min(getEndCoordinates()[0].getX(), getEndCoordinates()[1].getX()) <= intersection.getX()
				&& Math.min(getEndCoordinates()[0].getY(), getEndCoordinates()[1].getY()) <= intersection.getY()
				&& Math.max(getEndCoordinates()[0].getX(), getEndCoordinates()[1].getX()) >= intersection.getX()
				&& Math.max(getEndCoordinates()[0].getY(), getEndCoordinates()[1].getY()) >= intersection.getY();
	}

	private void setDistanceFromCrossSectionCenter() {
		super.setDistanceFromCrossSectionCenter(
				(getEndCoordinates()[0].getY() - Units.tan(angleWithBaseDiameter) * getEndCoordinates()[0].getX())
						* Units.cos(angleWithBaseDiameter));
	}	

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("\n===============\nWedge Damage Line ID: " + lineId);
		sb.append("\nAngle with base diameter: " + angleWithBaseDiameter);
		sb.append(super.toString() + "\n===============\n");
		return sb.toString();
	}
}
