package com.alamon.si_calculator.math.utils;

import java.util.Comparator;

import com.alamon.si_calculator.math.geometry.Pocket;

public class ChordBreakComparator implements Comparator<Pocket>	{
		@Override
		public int compare(Pocket arg0, Pocket arg1) {
			return arg0.getStartPoint() > arg1.getStartPoint() ? 1 :  arg0.getStartPoint() < arg1.getStartPoint() ? -1 : 0;
		}
		
	}