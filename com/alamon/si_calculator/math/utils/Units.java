package com.alamon.si_calculator.math.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

import com.alamon.resource_reader.json.LoadStaticResources;

public class Units {
	
	public static double cos(double deg)	{
		return Math.cos(deg * Math.PI/180);
	}
	
	public static double aSin(double sin)	{
		return (180 * Math.asin(sin)) / Math.PI;
	}
	public static double sin(double deg)	{
		return Math.sin(deg * Math.PI/180);
	}
	
	public static double tan(double deg)	{
		return Math.tan(deg * Math.PI/180);
	}
	
	public static double aTan (double tanDeg)	{
		return (Math.atan((tanDeg * Math.PI)/180));
		//return (Math.atan(tanDeg));
	}
	
	public static double cot(double deg)	{
		return 1 / tan(deg);		
	}
	
	public static double aCos(double cos)	{
		return (180 * Math.acos(cos)) / Math.PI;
	}
	
	/**
     * Truncate double variable passed decimal points to the number specified in precision field value setting from varConstantsArr.json.     *
     * @param fullDbl
     * or null to use precision field value setting from varConstantsArr.json
     * or any negative to use default value of 1
     * @return truncated double
     */
    public static double setDoublePrecision(Double fullDbl) {
        if (fullDbl == null) {
            return 0;
        } else if (fullDbl < 0)	{
        	return 1; //default
        }
        return BigDecimal.valueOf(fullDbl).setScale( getPrecisionValue(), RoundingMode.HALF_UP).doubleValue();
    }

    /**
     * Get precision/number of decimal points for double variables stored in precision field value setting in varConstantsArr.json
     *
     * @return
     */
    private static int getPrecisionValue() {        
        Map<String, String> criteria = new HashMap<>();
        criteria.put("name", "precision");
        String precisionStr = null;
        try {
            precisionStr = LoadStaticResources.getFileContentsByFileName("varConstantsArr.json").getElementsByFieldNameAndValue(criteria).get(0).get("value");
            System.out.println("precisionStr = " + precisionStr);
        } catch (Exception e) {
            return 2;
        }
        return InputValidators.validateIntInput(precisionStr);
    }
}
