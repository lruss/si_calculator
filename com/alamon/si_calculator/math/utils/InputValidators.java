package com.alamon.si_calculator.math.utils;

import java.util.regex.Pattern;

/**
 * Created by lruss on 7/3/2017.
 */
public class InputValidators {
    private static final Pattern DOUBLE_PATTERN = Pattern.compile(
            "[\\x00-\\x20]*[+-]?(NaN|Infinity|((((\\p{Digit}+)(\\.)?((\\p{Digit}+)?)" +
                    "([eE][+-]?(\\p{Digit}+))?)|(\\.((\\p{Digit}+))([eE][+-]?(\\p{Digit}+))?)|" +
                    "(((0[xX](\\p{XDigit}+)(\\.)?)|(0[xX](\\p{XDigit}+)?(\\.)(\\p{XDigit}+)))" +
                    "[pP][+-]?(\\p{Digit}+)))[fFdD]?))[\\x00-\\x20]*");
    private static final Pattern INTEGER_PATTERN = Pattern.compile("^\\d+(\\.\\d+)?");

    /**
     * Take a String, validate if it contains double, parse it, and return.
     * @param String input
     * @return Double if the value passed is indeed double, otherwise, null;
     */
    public static Double validateDoubleInput(String input)    {
        if(DOUBLE_PATTERN.matcher(input).matches())   {
            return Double.parseDouble(input);
        }
        return null;
    }

    /**
     * Take a String, validate if it contains int, parse it, and return.
     * @param String input
     * @return Integer if the value passed is indeed Integer, otherwise, null;
     */
    public static Integer validateIntInput(String input)   {
        if(INTEGER_PATTERN.matcher(input).matches())    {
            return Integer.parseInt(input);
        }
        return null;
    }
}
