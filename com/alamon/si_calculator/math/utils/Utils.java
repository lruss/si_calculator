package com.alamon.si_calculator.math.utils;

import com.alamon.si_calculator.math.geometry.Chord;
import com.alamon.si_calculator.math.geometry.Coordinates;

public class Utils {

	public static double getDistanceBetweenPointsByCoordinates(Coordinates[] coordinates) {
		if (coordinates == null || coordinates[0] == null || coordinates[1] == null) {
			return 0;
		}
		return Math.sqrt(Math.abs(Math.pow((coordinates[0].getY() - coordinates[1].getY()), 2)
				+ Math.pow((coordinates[0].getX() - coordinates[1].getX()), 2)));
	}

	public static double getDistanceBetweenPointsByCoordinates(Coordinates coordinates1, Coordinates coordinates2) {
		if (coordinates1 == null || coordinates2 == null) {
			return 0;
		}
		return Math.sqrt(Math.abs(Math.pow((coordinates1.getY() - coordinates2.getY()), 2)
				+ Math.pow((coordinates1.getX() - coordinates2.getX()), 2)));
	}

	public static double getChordLengthByAngleAndDiameter1(double angle, double diameter) {
		return diameter * Units.sin(angle / 2);
	}

	public static double getSegmentLengthByChordLengthAndDiameter(double diameter, double length) {
		return ((Math.PI * diameter) / 180) * Units.aSin(length / diameter);
	}

	/**
	 * 
	 * @param diameter
	 * @param angleBetweenRadiusesToSegmentEnds
	 * @return
	 */
	public static double getSegmentLengthBySideAngleAndDiameter(double diameter,
			double angleBetweenRadiusesToSegmentEnds) {
		return Math.PI * diameter * angleBetweenRadiusesToSegmentEnds / 360;
	}

	public static double getChordLengthByDiameterAndAngleBetweenRadiuses(double diameter, double angle) {
		return (Math.PI * diameter * angle) / 360;
	}

	public static double getLineAngleByEndPoints(Coordinates[] endPoints) {
		double ret = Units.aTan((endPoints[0].getY() - endPoints[1].getY())
				/ (endPoints[0].getX() - endPoints[1].getX()) * (180 / Math.PI));
		ret *= 180 / Math.PI;
		return ret; //LR TEST TEMP
	}

	/**
	 * NOTE: When using for any chord/line other than main cross section chord, make sure correct 
		 * angle to base diameter is set to chord parameter prior to further calculations
	 * @param chord - used to calculate offsetFromXaxis, if null or chord/line object not created yet, pass null
	 * 						and use return object to construct a Line or Chord intstance
	 * @param angleToBaseDiameter - if set to null, will use cross section general angle
	 * @param diameter
	 * @param circleCenterCoordiantes
	 * @return
	 */
	public static Coordinates[] calculateChordWithCircleIntesectionPoints(Chord chord, Double angleToBaseDiameter,
			double diameter, Coordinates circleCenterCoordiantes) {
		Double angle = null;
		Double offsetFromXaxis = null;
		if (chord == null) {
			offsetFromXaxis = 0.0;
			angle = angleToBaseDiameter;
		} else {
			angle = (angleToBaseDiameter == null ? chord.getCrossSection().getChordToBaseDiamAngle()
					: angleToBaseDiameter);
			offsetFromXaxis = chord.getDistanceFromCrossSectionCenter();
		}
		double circleCenX = 0;
		double circleCenY = 0;
		if (circleCenterCoordiantes != null) {
			circleCenX = circleCenterCoordiantes.getX();
			circleCenY = circleCenterCoordiantes.getY();
		}

		double a = 1 + Math.pow(Units.tan(angle), 2);
		double b = -2 * circleCenX + 2 * Units.tan(angle) * offsetFromXaxis - 2 * Units.tan(angle) * circleCenY;
		double c = Math.pow(circleCenX, 2) + Math.pow(offsetFromXaxis, 2) - 2 * offsetFromXaxis * circleCenY
				+ Math.pow(circleCenY, 2) - Math.pow((diameter / 2), 2);

		double x1 = (-b + Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / (2 * a);
		double x2 = (-b - Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / (2 * a);
		double y1 = Units.tan(angle) * x1 + offsetFromXaxis;
		double y2 = Units.tan(angle) * x2 + offsetFromXaxis;
		return new Coordinates[] { new Coordinates(x1, y1), new Coordinates(x2, y2) };
	}

	/**
	 * @param startCoordinates
	 * @param length - if not available, can use arbitrary value or null so the end coordinates returned
	 * 						 are used as an input parameter to create a Chord/Line object and then
	 * 						set end coordinates for example with calculateChordWithCircleIntesectionPoints method
	 * @param angleToBaseDiameter
	 * @return
	 */
	public static Coordinates getLineEndCoordinates(Coordinates startCoordinates, Double length,
			double angleToBaseDiameter) {
		if (length == null) {
			length = 10.0;
		}
		double endX = (startCoordinates.getX() - Units.cos(angleToBaseDiameter)) * length;
		double endY = (startCoordinates.getY() - Units.sin(angleToBaseDiameter)) * length;
		return new Coordinates(endX, endY);
	}
	
	public static double calculateArcLengthByDiameterAndAngle(double angle, double diameter)	{
		return (Math.PI * angle * diameter) / 360;
	}
	
	public static double calculateAngleByArcLengthAndDiameter(double arcLength, double diameter)	{
		return (360 * arcLength) / (Math.PI * diameter);
	}
}
